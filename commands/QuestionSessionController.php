<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Profil;
use app\models\JawabanSoal;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class QuestionSessionController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionApprove()
    {
        $profil = Profil::find()
            ->where(['has_soal_submitted'=>1, 'has_soal_approved'=>0])
            ->all();
        
        foreach ($profil as $key => $each) {
            $jawaban = JawabanSoal::find()
                ->where([
                    'profil_id'=>$each->id,
                    'penentu'=>1
                ])
                ->asArray()
                ->one();

            $arrPilihan = explode(',', $jawaban['jawaban']);

            if (in_array($each->posisi, ['Business Consultant', 'Manager Trainee']) ||in_array('Bussiness Consultant', $arrPilihan) || in_array('MT', $arrPilihan)) {
                $each->has_soal_approved = 1; // berhasil
                $each->save();
                echo $each->nama_lengkap . ' ' . "Berhasil.\n";
            } else {
                $each->has_soal_approved = 2; // gagal
                $each->save();
                echo $each->nama_lengkap . ' ' . "Gagal.\n";
            }
        }

        return ExitCode::OK;
    }
}
