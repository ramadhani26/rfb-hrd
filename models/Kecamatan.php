<?php

namespace app\models;

use Yii;
use app\components\AmayaActiveRecord;

class Kecamatan extends AmayaActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_kecamatan';
    }

}
