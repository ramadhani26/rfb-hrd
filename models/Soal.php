<?php

namespace app\models;

use Yii;
use app\models\Lookup;

/**
 * This is the model class for table "m_soal".
 *
 * @property int $id
 * @property string $pertanyaan
 * @property string $jenis text / radio / checklist / dropdown
 * @property int $urutan
 * @property int $required
 * @property string $l_type kalau jenis = radio / checklist / dropdown, ambil list dari lookup dengan lookup_type
 * @property string $additional_data
 * @property string $created_by
 * @property string $created_dt
 * @property string $updated_by
 * @property string $updated_dt
 * @property int $is_deleted
 * @property string $deleted_by
 * @property string $deleted_dt
 */
class Soal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_soal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pertanyaan'], 'required'],
            [['urutan', 'required', 'is_deleted'], 'integer'],
            [['additional_data'], 'string'],
            [['created_dt', 'updated_dt', 'deleted_dt', 'penentu'], 'safe'],
            [['pertanyaan', 'jenis', 'l_type', 'created_by', 'updated_by', 'deleted_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pertanyaan' => 'Pertanyaan',
            'jenis' => 'Jenis',
            'urutan' => 'Urutan',
            'required' => 'Required',
            'l_type' => 'L Type',
            'additional_data' => 'Additional Data',
            'created_by' => 'Created By',
            'created_dt' => 'Created Dt',
            'updated_by' => 'Updated By',
            'updated_dt' => 'Updated Dt',
            'is_deleted' => 'Is Deleted',
            'deleted_by' => 'Deleted By',
            'deleted_dt' => 'Deleted Dt',
        ];
    }

}
