<?php

namespace app\models;

use Yii;

class SetManagerForm extends \yii\base\Model
{
    public $id;
    public $manager_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'manager_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'manager_id' => 'SBC',
        ];
    }
}
