<?php

namespace app\models;

use Yii;
use yii2mod\user\models\UserModel as User;
use app\models\Lookup;
use app\models\JawabanSoal;
use app\models\Room;
use app\components\AmayaActiveRecord;

/**
 * This is the model class for table "m_profil".
 *
 * @property int $id
 * @property int $user_id
 * @property string $nama_lengkap
 * @property string $nomor_identitas
 * @property string $jenis_identitas
 * @property string $alamat_identitas
 * @property string $rt_identitas
 * @property string $rw_identitas
 * @property string $kelurahan_identitas
 * @property string $kecamatan_identitas
 * @property string $kota_identitas
 * @property string $provinsi_identitas
 * @property string $alamat_sekarang
 * @property string $rt_sekarang
 * @property string $rw_sekarang
 * @property string $kelurahan_sekarang
 * @property string $kecamatan_sekarang
 * @property string $kota_sekarang
 * @property string $provinsi_sekarang
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $status_perkawinan
 * @property string $no_telepon
 * @property string $no_handphone
 * @property string $ayah_kandung
 * @property string $ibu_kandung
 * @property string $email
 * @property string $keluarga_nama
 * @property string $keluarga_alamat
 * @property string $keluarga_no_telepon
 * @property string $kantor
 * @property string $kantor_cabang
 * @property string $nama_file
 * @property string $created_by
 * @property string $created_dt
 * @property string $updated_by
 * @property string $updated_dt
 * @property int $is_deleted
 * @property string $deleted_by
 * @property string $deleted_dt
 * @property string $manager_id
 */
class Profil extends AmayaActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_profil';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'nama_lengkap', 'alamat_sekarang', 'no_handphone'], 'required'],
            [['user_id', 'is_deleted'], 'integer'],
            [['alamat_identitas', 'alamat_sekarang', 'keluarga_alamat', 'nip'], 'string'],
            [
                [
                    'tanggal_lahir', 'created_dt', 'updated_dt', 'deleted_dt', 'manager_id', 'manager_id', 'nama_file', 
                    'posisi', 'request_to_recall', 'tanggal_berlaku', 'pendidikan_terakhir', 'agama',
                    'manager_room_id', 'no_telepon', 'umur'
                ], 'safe'],
            [['nama_lengkap', 'nomor_identitas', 'jenis_identitas', 'rt_identitas', 'rw_identitas', 'kelurahan_identitas', 'kecamatan_identitas', 'kota_identitas', 'provinsi_identitas', 'rt_sekarang', 'rw_sekarang', 'kelurahan_sekarang', 'kecamatan_sekarang', 'kota_sekarang', 'provinsi_sekarang', 'tempat_lahir', 'jenis_kelamin', 'status_perkawinan', 'no_telepon', 'no_handphone', 'ayah_kandung', 'ibu_kandung', 'email', 'keluarga_nama', 'keluarga_no_telepon', 'kantor', 'created_by', 'updated_by', 'deleted_by'], 'string', 'max' => 255],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getManager()
    {
        return $this->hasOne(self::className(), ['id' => 'manager_id']);
    }

    public function getLookupStatusProfil()
    {
        return $this->hasOne(Lookup::className(), ['id' => 'l_status_profil_id']);
    }

    public function getJawabanSoal()
    {
        return $this->hasMany(JawabanSoal::className(), ['id' => 'profil_id']);
    }

    public function getJawabanSoalSumberLowongan()
    {
        return $this->hasOne(JawabanSoal::className(), ['profil_id' => 'id'])->andOnCondition([
            'soal'=>'Dari mana anda mengetahui lowongan ini?']);
    }

    public function getManagerRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'manager_room_id']);
    }

    public function getFollowers()
    {
        return $this->hasMany(self::className(), ['id' => 'manager_id']);
    }
}
