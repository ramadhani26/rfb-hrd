<?php

namespace app\models;

use Yii;
use app\components\AmayaActiveRecord;

/**
 * This is the model class for table "t_jawaban_soal".
 *
 * @property int $id
 * @property int $profil_id
 * @property string $soal
 * @property string $jawaban
 * @property string $created_by
 * @property string $created_dt
 * @property string $updated_by
 * @property string $updated_dt
 * @property int $is_deleted
 * @property string $deleted_by
 * @property string $deleted_dt
 */
class JawabanSoal extends AmayaActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_jawaban_soal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['profil_id', 'is_deleted'], 'integer'],
            [['jawaban'], 'string'],
            [['created_dt', 'updated_dt', 'deleted_dt', 'penentu'], 'safe'],
            [['soal', 'created_by', 'updated_by', 'deleted_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profil_id' => 'Profil ID',
            'soal' => 'Soal',
            'jawaban' => 'Jawaban',
            'created_by' => 'Created By',
            'created_dt' => 'Created Dt',
            'updated_by' => 'Updated By',
            'updated_dt' => 'Updated Dt',
            'is_deleted' => 'Is Deleted',
            'deleted_by' => 'Deleted By',
            'deleted_dt' => 'Deleted Dt',
        ];
    }


    public function getProfil()
    {
        return $this->hasOne(self::className(), ['id' => 'profil_id']);
    }
}
