<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_room".
 *
 * @property int $id
 * @property string $nama_room
 * @property string $jadwal_test
 * @property string $created_by
 * @property string $created_dt
 * @property string $updated_by
 * @property string $updated_dt
 * @property int $is_deleted
 * @property string $deleted_by
 * @property string $deleted_dt
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_room';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_dt', 'updated_dt', 'deleted_dt'], 'safe'],
            [['is_deleted'], 'integer'],
            [['nama_room', 'jadwal_test', 'created_by', 'updated_by', 'deleted_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_room' => 'Nama Room',
            'jadwal_test' => 'Jadwal Test',
            'created_by' => 'Created By',
            'created_dt' => 'Created Dt',
            'updated_by' => 'Updated By',
            'updated_dt' => 'Updated Dt',
            'is_deleted' => 'Is Deleted',
            'deleted_by' => 'Deleted By',
            'deleted_dt' => 'Deleted Dt',
        ];
    }
}
