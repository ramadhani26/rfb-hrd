<?php

namespace app\models;

use Yii;

class FingerForm extends \yii\base\Model
{
    public $profil_id;
    public $nama_lengkap;
    public $jenis_identitas;
    public $nomor_identitas;
    public $tanggal_berlaku; //*
    public $alamat_identitas;
    public $rt_identitas;
    public $rw_identitas;
    public $kelurahan_identitas;
    public $kecamatan_identitas;
    public $kota_identitas;
    public $provinsi_identitas;
    public $alamat_sekarang;
    public $rt_sekarang;
    public $rw_sekarang;
    public $kelurahan_sekarang;
    public $kecamatan_sekarang;
    public $kota_sekarang;
    public $provinsi_sekarang;
    public $tempat_lahir;
    public $tanggal_lahir;
    public $pendidikan_terakhir; //*
    public $jenis_kelamin;
    public $status_perkawinan;
    public $agama; //*
    public $no_telepon;
    public $no_handphone;
    public $ayah_kandung;
    public $ibu_kandung;
    public $email;
    public $keluarga_nama;
    public $keluarga_alamat;
    public $keluarga_no_telepon;
    public $kantor;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'kantor',
                    'nama_lengkap',
                    'jenis_identitas',
                    'nomor_identitas',
                    'tanggal_berlaku', //*
                    'alamat_identitas',
                    'rt_identitas',
                    'rw_identitas',
                    'kelurahan_identitas',
                    'kecamatan_identitas',
                    'kota_identitas',
                    'provinsi_identitas',
                    'alamat_sekarang',
                    'rt_sekarang',
                    'rw_sekarang',
                    'kelurahan_sekarang',
                    'kecamatan_sekarang',
                    'kota_sekarang',
                    'provinsi_sekarang',
                    'tempat_lahir',
                    'tanggal_lahir',
                    'pendidikan_terakhir', //*
                    'jenis_kelamin',
                    'status_perkawinan',
                    'agama', //*
                    'no_telepon',
                    'no_handphone',
                    'ayah_kandung',
                    'ibu_kandung',
                    'email',
                    'keluarga_nama',
                    'keluarga_alamat',
                    'keluarga_no_telepon',
                ], 
                'required'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nama_lengkap'=>'Nama Lengkap',
            'jenis_identitas'=>'Jenis Identitas',
            'nomor_identitas'=>'Nomor Identitas',
            'tanggal_berlaku'=>'Tanggal Berlaku', //*
            'alamat_identitas'=>'Alamat (Sesuai Identitas)',
            'rt_identitas'=>'RT',
            'rw_identitas'=>'Rw',
            'kelurahan_identitas'=>'Kelurahan',
            'kecamatan_identitas'=>'Kecamatan',
            'kota_identitas'=>'Kota',
            'provinsi_identitas'=>'Provinsi',
            'alamat_sekarang'=>'Alamat Tinggal Sekarang',
            'rt_sekarang'=>'RT',
            'rw_sekarang'=>'RW',
            'kelurahan_sekarang'=>'Kelurahan',
            'kecamatan_sekarang'=>'Kecamatan',
            'kota_sekarang'=>'Kota',
            'provinsi_sekarang'=>'Provinsi',
            'tempat_lahir'=>'Tempat Lahir',
            'tanggal_lahir'=>'Tanggal Lahir / Umur',
            'pendidikan_terakhir'=>'Pendidikan Terakhir', //*
            'jenis_kelamin'=>'Jenis Kelamin',
            'status_perkawinan'=>'Status Perkawinan',
            'agama'=>'Agama', //*
            'no_telepon'=>'No Telepon',
            'no_handphone'=>'No Handphone',
            'ayah_kandung'=>'Ayah Kandung',
            'ibu_kandung'=>'Ibu Kandung',
            'email'=>'Email',
            'keluarga_nama'=>'Nama Keluarga',
            'keluarga_alamat'=>'Alamat Keluarga',
            'keluarga_no_telepon'=>'No Telepon Keluarga',
            'kantor'=>'Kantor Pusat / Cabang',
        ];
    }
}
