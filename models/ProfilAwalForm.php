<?php

namespace app\models;

use Yii;

class ProfilAwalForm extends \yii\base\Model
{
    public $nama_lengkap;
    public $alamat_sekarang;
    public $no_handphone;
    public $nama_file_cv;
    public $nama_file_ktp;
    public $nama_file_kk;
    public $nama_file_ijazah;
    public $nama_file_skck;
    public $posisi;
    public $posisi_lainnya;
    public $manager_room_id;

    public $tempat_lahir;
    public $tanggal_lahir;
    public $umur;
    public $jenis_kelamin;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_lengkap', 'alamat_sekarang', 'no_handphone', 'tempat_lahir', 'tanggal_lahir', 'umur', 'jenis_kelamin'], 'required'],
            [['nama_file_cv', 'nama_file_ktp', 'nama_file_kk', 'nama_file_ijazah', 'nama_file_skck', 'posisi', 'posisi_lainnya', 'manager_room_id'], 'safe'],
            [['nama_file_cv', 'nama_file_ktp', 'nama_file_kk', 'nama_file_ijazah', 'nama_file_skck', 'posisi', 'posisi_lainnya'], 'required', 'on'=>'trainee'],
            [['manager_room_id'], 'required', 'on'=>'manager'],
            [['nama_file_cv', 'nama_file_ktp', 'nama_file_kk', 'nama_file_ijazah', 'nama_file_skck',], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, pdf'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nama_lengkap' => 'Nama Lengkap',
            'alamat_sekarang' => 'Alamat Sekarang',
            'no_handphone' => 'No Handphone',
            'posisi' => 'Posisi',
            'manager_room_id' => 'Room',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'umur' => 'Umur (Otomatis)',
            'jenis_kelamin' => 'Jenis Kelamin',
            'nama_file_cv' => 'Upload CV',
            'nama_file_ktp' => 'Upload KTP',
            'nama_file_kk' => 'Upload Kartu Keluarga',
            'nama_file_ijazah' => 'Upload Ijazah',
            'nama_file_skck' => 'Upload SKCK',
        ];
    }
}
