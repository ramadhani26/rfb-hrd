<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_office".
 *
 * @property int $id
 * @property string $nama_kantor
 * @property string $alamat_kantor
 * @property string $no_telepon
 * @property string $fax
 * @property string $email
 * @property string $jenis
 * @property string $created_by
 * @property string $created_dt
 * @property string $updated_by
 * @property string $updated_dt
 * @property int $is_deleted
 * @property string $deleted_by
 * @property string $deleted_dt
 */
class Office extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_office';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_dt', 'updated_dt', 'deleted_dt'], 'safe'],
            [['is_deleted'], 'integer'],
            [['nama_kantor', 'alamat_kantor', 'no_telepon', 'fax', 'email', 'jenis', 'created_by', 'updated_by', 'deleted_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_kantor' => 'Nama Kantor',
            'alamat_kantor' => 'Alamat Kantor',
            'no_telepon' => 'No Telepon',
            'fax' => 'Fax',
            'email' => 'Email',
            'jenis' => 'Jenis',
            'created_by' => 'Created By',
            'created_dt' => 'Created Dt',
            'updated_by' => 'Updated By',
            'updated_dt' => 'Updated Dt',
            'is_deleted' => 'Is Deleted',
            'deleted_by' => 'Deleted By',
            'deleted_dt' => 'Deleted Dt',
        ];
    }
}
