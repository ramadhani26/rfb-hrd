<?php

namespace app\models;

use Yii;
use app\components\AmayaActiveRecord;

class Kelurahan extends AmayaActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_kelurahan';
    }

}
