<?php

return [
    'adminEmail' => 'info.hrdrifan@gmail.com',
    'senderEmail' => 'info.hrdrifan@gmail.com',
    'senderName' => 'rfb-bandung.com no reply',
    'user.passwordResetTokenExpire' => 3600,
];
