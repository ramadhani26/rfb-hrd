<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190914_175730_alter_profil_tgl_finger
 */
class m190914_175730_alter_profil_tgl_finger extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'tanggal_finger', Schema::TYPE_DATE . ' DEFAULT null AFTER kantor');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190914_175730_alter_profil_tgl_finger cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190914_175730_alter_profil_tgl_finger cannot be reverted.\n";

        return false;
    }
    */
}
