<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190914_015743_alter_profil_finger
 */
class m190914_015743_alter_profil_finger extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'tanggal_berlaku', Schema::TYPE_DATE . ' DEFAULT null AFTER jenis_identitas');
        $this->addColumn('m_profil', 'pendidikan_terakhir', Schema::TYPE_STRING . ' AFTER tanggal_lahir');
        $this->addColumn('m_profil', 'agama', Schema::TYPE_STRING . ' AFTER status_perkawinan');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190914_015743_alter_profil_finger cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190914_015743_alter_profil_finger cannot be reverted.\n";

        return false;
    }
    */
}
