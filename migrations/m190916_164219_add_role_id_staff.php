<?php

use yii\db\Migration;

/**
 * Class m190916_164219_add_role_id_staff
 */
class m190916_164219_add_role_id_staff extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->insert('user_role', [
            'name' => 'Staff',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190916_164219_add_role_id_staff cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190916_164219_add_role_id_staff cannot be reverted.\n";

        return false;
    }
    */
}
