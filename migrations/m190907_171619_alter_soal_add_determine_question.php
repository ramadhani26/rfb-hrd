<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190907_171619_alter_soal_add_determine_question
 */
class m190907_171619_alter_soal_add_determine_question extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_soal', 'penentu', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER l_type');

        // update all row to false
        $this->update('m_soal', ['penentu'=>0]);
        $this->update('m_soal', ['penentu'=>1], 'pertanyaan = :pertanyaan', [':pertanyaan'=>'anda diberi kesempatan untuk memilih 3 posisi yang terdapat di PT.BCD ?(Pilihan boleh tidak sesuai posisi yang di lamar ketika pendaftaran)']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('m_soal', 'penentu');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190907_171619_alter_soal_add_determine_question cannot be reverted.\n";

        return false;
    }
    */
}
