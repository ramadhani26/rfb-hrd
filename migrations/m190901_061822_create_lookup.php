<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190901_061822_create_lookup
 */
class m190901_061822_create_lookup extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $columns = [
            'id' => Schema::TYPE_PK . ' AUTO_INCREMENT',
            'type' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            // log field
            'created_by' => Schema::TYPE_STRING,
            'created_dt' => Schema::TYPE_DATETIME,
            'updated_by' => Schema::TYPE_STRING,
            'updated_dt' => Schema::TYPE_DATETIME,
            'is_deleted' => Schema::TYPE_BOOLEAN . " DEFAULT 0",
            'deleted_by' => Schema::TYPE_STRING,
            'deleted_dt' => Schema::TYPE_DATETIME
        ];
        $this->createTable('m_lookup', $columns);

        $this->insert('m_lookup', [
            'id'=>1,
            'type'=>'status_profil',
            'name'=>'Tidak aktif',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'id'=>2,
            'type'=>'status_profil',
            'name'=>'Aktif',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('m_lookup');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190901_061822_create_lookup cannot be reverted.\n";

        return false;
    }
    */
}
