<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190903_175845_alter_profil_add_soal
 */
class m190903_175845_alter_profil_add_soal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'has_soal_submitted', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER nama_file');
        $this->addColumn('m_profil', 'has_soal_approved', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER has_soal_submitted');

        // update all row to false
        $this->update('m_profil', ['has_soal_submitted'=>0, 'has_soal_approved'=>0]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('m_profil', 'has_soal_submitted');
        $this->dropColumn('m_profil', 'has_soal_approved');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190903_175845_alter_profil_add_soal cannot be reverted.\n";

        return false;
    }
    */
}
