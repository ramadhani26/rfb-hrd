<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190902_152828_create_soal
 */
class m190902_152828_create_soal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $columns = [
            'id' => Schema::TYPE_PK . ' AUTO_INCREMENT',
            'pertanyaan' => Schema::TYPE_STRING . ' NOT NULL',
            'jenis' => Schema::TYPE_STRING . " COMMENT 'text / radio / checklist / dropdown'",
            'urutan' => Schema::TYPE_INTEGER,
            'required' => Schema::TYPE_BOOLEAN,
            'l_type' => Schema::TYPE_STRING . " COMMENT 'kalau jenis = radio / checklist / dropdown, ambil list dari lookup dengan lookup_type'",
            'additional_data' => Schema::TYPE_TEXT,
            // log field
            'created_by' => Schema::TYPE_STRING,
            'created_dt' => Schema::TYPE_DATETIME,
            'updated_by' => Schema::TYPE_STRING,
            'updated_dt' => Schema::TYPE_DATETIME,
            'is_deleted' => Schema::TYPE_BOOLEAN . " DEFAULT 0",
            'deleted_by' => Schema::TYPE_STRING,
            'deleted_dt' => Schema::TYPE_DATETIME
        ];
        $this->createTable('m_soal', $columns);

        $this->insert('m_soal', [
            'pertanyaan'=>'Apa itu Business Consultant ?',
            'jenis' => 'text',
            'urutan' => 1,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Apa jobdesc dari seorang bussiness Consultant ?',
            'jenis' => 'text',
            'urutan' => 2,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Apa hubungan posisi yang anda lamar dengan seorang Bussiness Consultan ? ( kosongkan bila posisi Bussiness Consultant yang anda lamar )',
            'jenis' => 'text',
            'urutan' => 3,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Apa saja perbedaan peluang seorang Bussines Consultan dengan posisi yang anda lamar ?',
            'jenis' => 'text',
            'urutan' => 4,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Sebutkan dan jelaskan secara detail sumber penghasilan seorang Bussiness Consultant ?',
            'jenis' => 'text',
            'urutan' => 5,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Jelaskan peluang keuntungan transaksi di PT.BCD ?',
            'jenis' => 'text',
            'urutan' => 6,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Apa saja produk-produk BBJ dan sebutkan 5 produk yang terdapat di PT.BCD ?',
            'jenis' => 'text',
            'urutan' => 7,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Jelaskan secara signifikan mekanisme transaksi di perdagangan berjangka ?',
            'jenis' => 'text',
            'urutan' => 8,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);


        //konten pilihan untuk soal nomor 9
        $this->insert('m_lookup', [
            'type'=>'soal_9',
            'name'=>'Pendapatan standart',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_9',
            'name'=>'pendapatan tak terbatas',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_9',
            'name'=>'Jenjang Karir',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_9',
            'name'=>'Menjadi trander',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_9',
            'name'=>'Menjadi Investor',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_9',
            'name'=>'Menjadi InvestorTrander',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_9',
            'name'=>'Menjadi wakil pialang berjangka',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Peluang apa yang ingin anda dapatkan di PT.BCD ( bisa lebih dari 1) Jelaskan alasan anda !',
            'jenis' => 'checkboxlist',
            'urutan' => 9,
            'required' => 0,
            'l_type' => 'soal_9',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);


        $this->insert('m_lookup', [
            'type'=>'soal_10',
            'name'=>'Bussiness Consultant',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_10',
            'name'=>'MT',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_10',
            'name'=>'HRD',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_10',
            'name'=>'Administrasi',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_10',
            'name'=>'IT',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_10',
            'name'=>'Secretary',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_10',
            'name'=>'Receiptionist',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_10',
            'name'=>'Assistant Manager',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            'type'=>'soal_10',
            'name'=>'Finance',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_soal', [
            'pertanyaan'=>'anda diberi kesempatan untuk memilih 3 posisi yang terdapat di PT.BCD ?(Pilihan boleh tidak sesuai posisi yang di lamar ketika pendaftaran)',
            'jenis' => 'checkboxlist',
            'urutan' => 10,
            'required' => 0,
            'l_type' => 'soal_10',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_soal', [
            'pertanyaan'=>'Pilihan 1',
            'jenis' => 'dropdownlist',
            'urutan' => 11,
            'required' => 0,
            'l_type' => 'soal_10',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Alasan',
            'jenis' => 'text',
            'urutan' => 12,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Pilihan 2',
            'jenis' => 'dropdownlist',
            'urutan' => 13,
            'required' => 0,
            'l_type' => 'soal_10',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Alasan',
            'jenis' => 'text',
            'urutan' => 14,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Pilihan 3',
            'jenis' => 'dropdownlist',
            'urutan' => 15,
            'required' => 0,
            'l_type' => 'soal_10',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_soal', [
            'pertanyaan'=>'Alasan',
            'jenis' => 'text',
            'urutan' => 16,
            'required' => 0,
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('m_soal');
        $this->delete('m_lookup', "type in ('soal_9', 'soal_10')");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190902_152828_create_soal cannot be reverted.\n";

        return false;
    }
    */
}
