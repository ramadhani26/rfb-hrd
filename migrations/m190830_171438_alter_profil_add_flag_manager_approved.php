<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190830_171438_alter_profil_add_flag_manager_approved
 */
class m190830_171438_alter_profil_add_flag_manager_approved extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'manager_approved', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER manager_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('m_profil', 'manager_approved');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190830_171438_alter_profil_add_flag_manager_approved cannot be reverted.\n";

        return false;
    }
    */
}
