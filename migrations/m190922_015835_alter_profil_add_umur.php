<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190922_015835_alter_profil_add_umur
 */
class m190922_015835_alter_profil_add_umur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'umur', Schema::TYPE_STRING . ' AFTER tanggal_lahir');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190922_015835_alter_profil_add_umur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190922_015835_alter_profil_add_umur cannot be reverted.\n";

        return false;
    }
    */
}
