<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190907_184558_alter_profil_add_request_to_recall
 */
class m190907_184558_alter_profil_add_request_to_recall extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'request_to_recall', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER has_soal_approved');
        $this->addColumn('m_profil', 'response_recall', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER request_to_recall');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('m_profil', 'request_to_recall');
        $this->dropColumn('m_profil', 'response_recall');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190907_184558_alter_profil_add_request_to_recall cannot be reverted.\n";

        return false;
    }
    */
}
