<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190825_160616_alter_user
 */
class m190825_160616_alter_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'user_role_id', Schema::TYPE_INTEGER . ' DEFAULT 5');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'user_role_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190825_160616_alter_user cannot be reverted.\n";

        return false;
    }
    */
}
