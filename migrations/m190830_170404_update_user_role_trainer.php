<?php

use yii\db\Migration;

/**
 * Class m190830_170404_update_user_role_trainer
 */
class m190830_170404_update_user_role_trainer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('user_role', ['name'=>'Trainee'], 'id = 5');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190830_170404_update_user_role_trainer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190830_170404_update_user_role_trainer cannot be reverted.\n";

        return false;
    }
    */
}
