<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190919_161519_alter_profil_add_has_soal_showed
 */
class m190919_161519_alter_profil_add_has_soal_showed extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'has_soal_showed', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER nama_file');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190919_161519_alter_profil_add_has_soal_showed cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190919_161519_alter_profil_add_has_soal_showed cannot be reverted.\n";

        return false;
    }
    */
}
