<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190916_023204_alter_profil_add_room
 */
class m190916_023204_alter_profil_add_room extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'manager_room_id', Schema::TYPE_INTEGER . ' DEFAULT null AFTER nip');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190916_023204_alter_profil_add_room cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190916_023204_alter_profil_add_room cannot be reverted.\n";

        return false;
    }
    */
}
