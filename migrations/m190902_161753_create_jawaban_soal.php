<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190902_161753_create_jawaban_soal
 */
class m190902_161753_create_jawaban_soal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $columns = [
            'id' => Schema::TYPE_PK . ' AUTO_INCREMENT',
            'profil_id' => Schema::TYPE_INTEGER,
            'soal' => Schema::TYPE_STRING,
            'jawaban' => Schema::TYPE_TEXT,
            // log field
            'created_by' => Schema::TYPE_STRING,
            'created_dt' => Schema::TYPE_DATETIME,
            'updated_by' => Schema::TYPE_STRING,
            'updated_dt' => Schema::TYPE_DATETIME,
            'is_deleted' => Schema::TYPE_BOOLEAN . " DEFAULT 0",
            'deleted_by' => Schema::TYPE_STRING,
            'deleted_dt' => Schema::TYPE_DATETIME
        ];
        $this->createTable('t_jawaban_soal', $columns);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('t_jawaban_soal');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190902_161753_create_jawaban_soal cannot be reverted.\n";

        return false;
    }
    */
}
