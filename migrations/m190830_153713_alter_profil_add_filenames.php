<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190830_153713_alter_profil_add_filenames
 */
class m190830_153713_alter_profil_add_filenames extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'nama_file', Schema::TYPE_TEXT . ' AFTER kantor_cabang');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('m_profil', 'nama_file');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190830_153713_alter_profil_add_filenames cannot be reverted.\n";

        return false;
    }
    */
}
