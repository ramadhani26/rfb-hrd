<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190914_011755_create_office
 */
class m190914_011755_create_office extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $columns = [
            'id' => Schema::TYPE_PK . ' AUTO_INCREMENT',
            'nama_kantor' => Schema::TYPE_STRING,
            'alamat_kantor' => Schema::TYPE_STRING,
            'no_telepon' => Schema::TYPE_STRING,
            'fax' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING,
            'jenis' => Schema::TYPE_STRING, // pusat atau cabang
            // log field
            'created_by' => Schema::TYPE_STRING,
            'created_dt' => Schema::TYPE_DATETIME,
            'updated_by' => Schema::TYPE_STRING,
            'updated_dt' => Schema::TYPE_DATETIME,
            'is_deleted' => Schema::TYPE_BOOLEAN . " DEFAULT 0",
            'deleted_by' => Schema::TYPE_STRING,
            'deleted_dt' => Schema::TYPE_DATETIME
        ];
        $this->createTable('m_office', $columns);


        $this->insert('m_office', [
            'nama_kantor' => 'Kantor Pusat',
            'alamat_kantor' => 'AXA Tower Kuningan City Lt. 30, Jl. Prof. DR. Satrio Kav.18, Kuningan Setia Budi, Jakarta 12940',
            'no_telepon' => '(021) 3005 6300',
            'fax' => '(021) 3005 6200',
            'email' => 'corporate@rifan-financindo-berjangka.co.id',
            'jenis' => 'Pusat',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_office', [
            'nama_kantor' => 'Cabang JAKARTA - DBS TOWER',
            'alamat_kantor' => 'DBS Bank Tower LT. 14, CIPUTRA WORLD I Jl. Prof. DR. Satrio Kav. 3-5, Jakarta Selatan 12940',
            'no_telepon' => '(021) 2988 8700',
            'fax' => '(021) 2988 8701',
            'email' => 'compliance.dbs@rifan-financindo-berjangka.co.id',
            'jenis' => 'Cabang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_office', [
            'nama_kantor' => 'Cabang MEDAN',
            'alamat_kantor' => 'Best & Grow Tower (J.W.MARRIOT) Lt. 12, Unit # 1205 - 1209 Jl. Putri Hijau No. 10, Medan 20111',
            'no_telepon' => '(061) 414 0575',
            'fax' => '(061) 414 0576',
            'email' => 'compliance.mdn@rifan-financindo-berjangka.co.id',
            'jenis' => 'Cabang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_office', [
            'nama_kantor' => 'Cabang SEMARANG',
            'alamat_kantor' => 'Ruko S. Parman Corner Unit 5 & 6 Jl. S. Parman No. 47A, Semarang 50231',
            'no_telepon' => '(024) 850 8868',
            'fax' => '(024) 850 8869',
            'email' => 'compliance.smg@rifan-financindo-berjangka.co.id',
            'jenis' => 'Cabang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_office', [
            'nama_kantor' => 'Cabang SOLO',
            'alamat_kantor' => 'Ruko Solo Square No. 5, 6, dan 7 Jl. Slamet Riyadi No. 451 - 455, Solo 57145',
            'no_telepon' => '(0271) 738 111',
            'fax' => '(0271) 738 222',
            'email' => 'compliance.solo@rifan-financindo-berjangka.co.id',
            'jenis' => 'Cabang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_office', [
            'nama_kantor' => 'Cabang SURABAYA',
            'alamat_kantor' => 'Sinar Mas Land Plaza Lt. 16 ( d/h : Gedung Wisma BII ) Jl. Pemuda No. 60 - 70, Surabaya 60271',
            'no_telepon' => '(031) 534 9800',
            'fax' => '(031) 534 7800',
            'email' => 'compliance.sby@rifan-financindo-berjangka.co.id',
            'jenis' => 'Cabang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_office', [
            'nama_kantor' => 'Cabang BANDUNG',
            'alamat_kantor' => 'Wisma Bumi Putra Lt. 3 Suite # 302 - 307 Jl. Asia Afrika No. 141 - 149, Bandung 40112',
            'no_telepon' => '(022) 422 4288',
            'fax' => '(022) 422 4577',
            'email' => 'compliance.bdg@rifan-financindo-berjangka.co.id',
            'jenis' => 'Cabang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_office', [
            'nama_kantor' => 'Cabang PALEMBANG',
            'alamat_kantor' => 'Kompleks Pertokoan Palembang Square Unit 112.113.115 Jl. Kampus POM IX, Palembang 30137',
            'no_telepon' => '(0711) 380 555',
            'fax' => '(0711) 380 666',
            'email' => 'compliance.plm@rifan-financindo-berjangka.co.id',
            'jenis' => 'Cabang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_office', [
            'nama_kantor' => 'Cabang PEKANBARU',
            'alamat_kantor' => 'Gedung Pondasi Siabu, Lantai 1 & 2 Jl. Jend. Sudirman No. 211, Pekanbaru 28282',
            'no_telepon' => '(0761) 7870018',
            'fax' => '(0761) 7870019',
            'email' => 'compliance.pku@rifan-financindo-berjangka.co.id',
            'jenis' => 'Cabang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->insert('m_office', [
            'nama_kantor' => 'Cabang YOGYAKARTA',
            'alamat_kantor' => 'Ruko B1, B2, B3, dan B5 Malioboro City Jl. Laksda Adisucipto KM.8 Tambakbayan, Caturtunggal Depok Sleman - Yogyakarta 55281',
            'no_telepon' => '(0274) 280 3111',
            'fax' => '(0274) 280 3222',
            'email' => 'compliance.ygy@rifan-financindo-berjangka.co.id',
            'jenis' => 'Cabang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('m_office');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190914_011755_create_office cannot be reverted.\n";

        return false;
    }
    */
}
