<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190907_172525_alter_jawaban_soal_add_determine_question
 */
class m190907_172525_alter_jawaban_soal_add_determine_question extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('t_jawaban_soal', 'penentu', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER jawaban');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('t_jawaban_soal', 'penentu');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190907_172525_alter_jawaban_soal_add_determine_question cannot be reverted.\n";

        return false;
    }
    */
}
