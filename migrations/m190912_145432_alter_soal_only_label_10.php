<?php

use yii\db\Migration;

/**
 * Class m190912_145432_alter_soal_only_label_10
 */
class m190912_145432_alter_soal_only_label_10 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('m_soal', ['jenis'=>'label', 'penentu'=>0], ['urutan'=>10]);

        $this->update('m_soal', ['penentu'=>1], ['pertanyaan'=>['Pilihan 1', 'Pilihan 2', 'Pilihan 3']]);

        $this->update('m_soal', ['required'=>1]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190912_145432_alter_soal_only_label_10 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190912_145432_alter_soal_only_label_10 cannot be reverted.\n";

        return false;
    }
    */
}
