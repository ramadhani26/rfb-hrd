<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190825_163002_create_profile
 */
class m190825_163002_create_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $columns = [
            'id' => Schema::TYPE_PK . ' AUTO_INCREMENT',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'nama_lengkap' => Schema::TYPE_STRING . ' NOT NULL',
            'nomor_identitas' => Schema::TYPE_STRING,
            'jenis_identitas' => Schema::TYPE_STRING,
            'alamat_identitas' => Schema::TYPE_TEXT,
            'rt_identitas' => Schema::TYPE_STRING,
            'rw_identitas' => Schema::TYPE_STRING,
            'kelurahan_identitas' => Schema::TYPE_STRING,
            'kecamatan_identitas' => Schema::TYPE_STRING,
            'kota_identitas' => Schema::TYPE_STRING,
            'provinsi_identitas' => Schema::TYPE_STRING,
            'alamat_sekarang' => Schema::TYPE_TEXT,
            'rt_sekarang' => Schema::TYPE_STRING,
            'rw_sekarang' => Schema::TYPE_STRING,
            'kelurahan_sekarang' => Schema::TYPE_STRING,
            'kecamatan_sekarang' => Schema::TYPE_STRING,
            'kota_sekarang' => Schema::TYPE_STRING,
            'provinsi_sekarang' => Schema::TYPE_STRING,
            'tempat_lahir' => Schema::TYPE_STRING,
            'tanggal_lahir' => Schema::TYPE_DATE,
            'jenis_kelamin' => Schema::TYPE_STRING,
            'status_perkawinan' => Schema::TYPE_STRING,
            'no_telepon' => Schema::TYPE_STRING,
            'no_handphone' => Schema::TYPE_STRING,
            'ayah_kandung' => Schema::TYPE_STRING,
            'ibu_kandung' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING,
            'keluarga_nama' => Schema::TYPE_STRING,
            'keluarga_alamat' => Schema::TYPE_TEXT,
            'keluarga_no_telepon' => Schema::TYPE_STRING,
            'kantor_pusat' => Schema::TYPE_STRING,
            'kantor_cabang' => Schema::TYPE_STRING,
            // log field
            'created_by' => Schema::TYPE_STRING,
            'created_dt' => Schema::TYPE_DATETIME,
            'updated_by' => Schema::TYPE_STRING,
            'updated_dt' => Schema::TYPE_DATETIME,
            'is_deleted' => Schema::TYPE_BOOLEAN . " DEFAULT 0",
            'deleted_by' => Schema::TYPE_STRING,
            'deleted_dt' => Schema::TYPE_DATETIME
        ];
        $this->createTable('m_profil', $columns);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('m_profil');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190825_163002_create_profile cannot be reverted.\n";

        return false;
    }
    */
}
