<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190916_022413_create_room
 */
class m190916_022413_create_room extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $columns = [
            'id' => Schema::TYPE_PK . ' AUTO_INCREMENT',
            'nama_room' => Schema::TYPE_STRING,
            'jadwal_test' => Schema::TYPE_STRING,
            // log field
            'created_by' => Schema::TYPE_STRING,
            'created_dt' => Schema::TYPE_DATETIME,
            'updated_by' => Schema::TYPE_STRING,
            'updated_dt' => Schema::TYPE_DATETIME,
            'is_deleted' => Schema::TYPE_BOOLEAN . " DEFAULT 0",
            'deleted_by' => Schema::TYPE_STRING,
            'deleted_dt' => Schema::TYPE_DATETIME
        ];
        $this->createTable('m_room', $columns);

        $this->insert('m_room', [
            'nama_room' => 'RF05',
            'jadwal_test' => 'Pagi',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_room', [
            'nama_room' => 'RF07',
            'jadwal_test' => 'Pagi',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_room', [
            'nama_room' => 'RF08',
            'jadwal_test' => 'Siang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_room', [
            'nama_room' => 'RF10',
            'jadwal_test' => 'Siang',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('m_room');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190916_022413_create_room cannot be reverted.\n";

        return false;
    }
    */
}
