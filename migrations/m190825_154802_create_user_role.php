<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190825_154802_create_user_role
 */
class m190825_154802_create_user_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $columns = [
            'id' => Schema::TYPE_PK . ' AUTO_INCREMENT',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            // log field
            'created_by' => Schema::TYPE_STRING,
            'created_dt' => Schema::TYPE_DATETIME,
            'updated_by' => Schema::TYPE_STRING,
            'updated_dt' => Schema::TYPE_DATETIME,
            'is_deleted' => Schema::TYPE_BOOLEAN . " DEFAULT 0",
            'deleted_by' => Schema::TYPE_STRING,
            'deleted_dt' => Schema::TYPE_DATETIME
        ];
        $this->createTable('user_role', $columns);

        $this->insert('user_role', [
            'id'=>1,
            'name'=>'Super Admin',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('user_role', [
            'id'=>2,
            'name'=>'Head Manager',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('user_role', [
            'id'=>3,
            'name'=>'Manager',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('user_role', [
            'id'=>4,
            'name'=>'HRD',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('user_role', [
            'id'=>5,
            'name'=>'Trainer',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_role');
        echo "table user_role has been removed.\n";
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190825_154802_create_user_role cannot be reverted.\n";

        return false;
    }
    */
}
