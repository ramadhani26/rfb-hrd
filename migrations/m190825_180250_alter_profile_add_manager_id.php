<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190825_180250_alter_profile_add_manager_id
 */
class m190825_180250_alter_profile_add_manager_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'manager_id', Schema::TYPE_INTEGER . ' AFTER user_id');
        $this->addColumn('m_profil', 'nip', Schema::TYPE_STRING . ' AFTER manager_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('m_profil', 'manager_id');
        $this->dropColumn('m_profil', 'nip');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190825_180250_alter_profile_add_manager_id cannot be reverted.\n";

        return false;
    }
    */
}
