<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190901_061739_alter_profil_add_status
 */
class m190901_061739_alter_profil_add_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn('m_profil', 'l_status_profil_id', Schema::TYPE_BOOLEAN . ' DEFAULT 1 AFTER manager_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('m_profil', 'l_status_profil_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190901_061739_alter_profil_add_status cannot be reverted.\n";

        return false;
    }
    */
}
