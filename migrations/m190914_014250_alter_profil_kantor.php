<?php

use yii\db\Migration;

/**
 * Class m190914_014250_alter_profil_kantor
 */
class m190914_014250_alter_profil_kantor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('m_profil', 'kantor_pusat', 'kantor');
        $this->dropColumn('m_profil', 'kantor_cabang');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190914_014250_alter_profil_kantor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190914_014250_alter_profil_kantor cannot be reverted.\n";

        return false;
    }
    */
}
