<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190916_165222_alter_profil_add_is_lolos
 */
class m190916_165222_alter_profil_add_is_lolos extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'is_lolos', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER drop_manager');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190916_165222_alter_profil_add_is_lolos cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190916_165222_alter_profil_add_is_lolos cannot be reverted.\n";

        return false;
    }
    */
}
