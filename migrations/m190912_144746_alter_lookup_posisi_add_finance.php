<?php

use yii\db\Migration;
use yii\db\Schema;


/**
 * Class m190912_144746_alter_lookup_posisi_add_finance
 */
class m190912_144746_alter_lookup_posisi_add_finance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('m_lookup', [
            'type'=>'posisi',
            'name' => 'Finance',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);

        $this->update('m_lookup', ['name'=>'Admin Operasional'], ['type'=>'posisi', 'name'=>'Admin']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190912_144746_alter_lookup_posisi_add_finance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190912_144746_alter_lookup_posisi_add_finance cannot be reverted.\n";

        return false;
    }
    */
}
