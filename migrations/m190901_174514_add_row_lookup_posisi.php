<?php

use yii\db\Migration;

/**
 * Class m190901_174514_add_row_lookup_posisi
 */
class m190901_174514_add_row_lookup_posisi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('m_lookup', [
            'id'=>11,
            'type'=>'posisi',
            'name'=>'Manager Trainee',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            // 'id'=>11,
            'type'=>'posisi',
            'name'=>'Business Consultant',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
        $this->insert('m_lookup', [
            // 'id'=>11,
            'type'=>'posisi',
            'name'=>'Admin',
            'created_by'=>'migration',
            'created_dt'=>date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190901_174514_add_row_lookup_posisi cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190901_174514_add_row_lookup_posisi cannot be reverted.\n";

        return false;
    }
    */
}
