<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190916_155653_alter_profil_drop_manager
 */
class m190916_155653_alter_profil_drop_manager extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'drop_manager', Schema::TYPE_BOOLEAN . ' DEFAULT 0 AFTER response_recall');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190916_155653_alter_profil_drop_manager cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190916_155653_alter_profil_drop_manager cannot be reverted.\n";

        return false;
    }
    */
}
