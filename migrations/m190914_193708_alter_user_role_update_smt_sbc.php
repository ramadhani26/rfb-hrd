<?php

use yii\db\Migration;

/**
 * Class m190914_193708_alter_user_role_update_smt_sbc
 */
class m190914_193708_alter_user_role_update_smt_sbc extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('user_role', ['name'=>'SBC'], ['name'=>'SMT']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190914_193708_alter_user_role_update_smt_sbc cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190914_193708_alter_user_role_update_smt_sbc cannot be reverted.\n";

        return false;
    }
    */
}
