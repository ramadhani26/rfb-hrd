<?php

use yii\db\Migration;

/**
 * Class m190912_154400_update_role_manager_smt
 */
class m190912_154400_update_role_manager_smt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('user_role', ['name'=>'SMT'], ['name'=>'Manager']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190912_154400_update_role_manager_smt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190912_154400_update_role_manager_smt cannot be reverted.\n";

        return false;
    }
    */
}
