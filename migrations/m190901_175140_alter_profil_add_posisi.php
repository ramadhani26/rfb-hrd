<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190901_175140_alter_profil_add_posisi
 */
class m190901_175140_alter_profil_add_posisi extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('m_profil', 'posisi', Schema::TYPE_STRING . ' AFTER manager_approved');
        $this->alterColumn('m_profil', 'l_status_profil_id', Schema::TYPE_INTEGER);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('m_profil', 'posisi');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190901_175140_alter_profil_add_posisi cannot be reverted.\n";

        return false;
    }
    */
}
