<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Profil;
use yii2mod\user\models\UserModel;
use yii2mod\user\actions\Action;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'login' => [
                'class' => 'yii2mod\user\actions\LoginAction'
            ],
            'logout' => [
                'class' => 'yii2mod\user\actions\LogoutAction'
            ],
            'signup' => [
                'class' => 'yii2mod\user\actions\SignupAction'
            ],
            'request-password-reset' => [
                'class' => 'yii2mod\user\actions\RequestPasswordResetAction'
            ],
            'password-reset' => [
                'class' => 'yii2mod\user\actions\PasswordResetAction'
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            $model = new LoginForm();
            return $this->redirect('/site/front');
        }

        $profil = Profil::find()
            ->where(['user_id'=>Yii::$app->user->id])
            ->one();
        if (!$profil) {
            return $this->redirect('/profil/create');
        }
        
        $role_id = Yii::$app->user->identity->user_role_id;
        if ($role_id == 4) { // HRD
            return $this->redirect('/dashboard/index');
        }

        return $this->render('index');
    }

    public function actionFront()
    {
        $this->layout = '@app/views/adminLTE/layouts/main-front';
        return $this->render('front');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        // echo(123);
        // die;
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function action403()
    {
        return $this->render('403');
    }


    public function actionActivate($token)
    {
        $user = UserModel::find()
            ->andWhere(['password_reset_token' => $token])
            ->one();
        if ($user) {
            $user->status = 1;
            $user->removePasswordResetToken();

            // if (Yii::$app->getUser()->login($user)) {
            if ($user->save()) {
                Yii::$app->getSession()->setFlash('success', 'Akun berhasil diaktifkan. Silakan login kembali.');
                // return $this->redirect(Yii::$app->getUser()->getReturnUrl());
            } else {
                Yii::$app->getSession()->setFlash('error', 'Akun gagal diaktifkan');
            }
            return $this->redirect('/site/login');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Token expired.');
            return $this->redirect('/site/login');
        }
    }

    public function actionSistemPenerimaanKaryawan()
    {
        return $this->render('spk');
    }

    public function actionProduk()
    {
        return $this->render('produk');
    }

    public function actionLegalitas()
    {
        return $this->render('legalitas');
    }

    public function actionKegiatanKami()
    {
        return $this->render('kegiatan-kami');
    }
}
