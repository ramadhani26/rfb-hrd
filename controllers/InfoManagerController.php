<?php

namespace app\controllers;

use Yii;
use app\components\AmayaController;
use app\components\AmayaHelpers;
use \app\models\Profil;
use yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use yii2mod\user\models\UserModel;
use \app\models\SetManagerForm;

class InfoManagerController extends AmayaController {

    public $user;
    public function init() {
        if (Yii::$app->user->isGuest) return ;
        $this->user = Yii::$app->user->identity;
        if ($this->user->user_role_id != 4) {
            return $this->redirect('/site/403');
        }
    }
    
    public function actionIndex()
    {
        $title = 'Informasi SBC & MT/BC';

        return $this->render('index', get_defined_vars());
    }

    public function actionGetData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $model = Profil::find()
            ->joinWith(['lookupStatusProfil', 'user', 'managerRoom'])
            ->where(['user_role_id'=>3, 'm_profil.is_deleted'=>0])
            ->orderBy(['nama_lengkap'=>SORT_ASC])
            ->asArray()->all();

        $data = [];
        foreach ($model as $key => $each) {
            $data[$key] = $each;
            $data[$key]['ruangan'] = $each['managerRoom'] ? $each['managerRoom']['nama_room'] : '-';

            $aksi = [];
            $aksi[] = Html::a("Detail SBC", '/info-manager/detail-manager?id='.$each['id'], [
                'class'=>'btn btn-xs btn-info btn-detail-manager',
                'data-id'=>$each['id'],
            ]);

            $data[$key]['aksi'] = implode(' ', $aksi);
            $data[$key]['akun'] = 'testing';
        }

        return ['data'=>$data];
    }


    public function actionDetailManager($id)
    {
        $title = 'Informasi Detail SBC';

        $profil = Profil::find()
            ->joinWith('managerRoom')
            ->where(['m_profil.id'=>$id])
            ->asArray()->one();

        if (!$profil) {
            return $this->redirect("/site/404");
        }

        $followers = Profil::find()
            ->where(['manager_id'=>$id])
            ->asArray()->all();

        return $this->render('detail-manager', get_defined_vars());
    }

    public function actionGetDataAnggota($id) 
    {

        Yii::$app->response->format = Response::FORMAT_JSON;
        $listAnggota = Profil::find()
            ->where([
                'manager_id'=>$id,
                'manager_approved'=>1,
                'is_deleted'=>0,
            ])->asArray()->all();

        $data = [];
        foreach ($listAnggota as $key => $anggota) {
            $data[$key] = $anggota;

            $tempSts = [];
            if (!$anggota['is_lolos']) {
                if ($anggota['manager_approved'] == 1)  {
                    $tempSts[] = '<i class="fa fa-star"></i> ';
                } else {
                    $tempSts[] = '<i class="fa fa-star-o"></i> ';
                }

                if ($anggota['has_soal_submitted'] == 1) {
                    if ($anggota['has_soal_approved'] == 1) {
                        $tempSts[] = '<i class="fa fa-star"></i> ';
                    } elseif ($anggota['has_soal_approved'] == 2) { // gagal di sesi soal
                        if ($anggota['response_recall'] != 1) {
                            $tempSts[] = '<i class="fa fa-star-half-o"></i> ';
                        } else {
                            $tempSts[] = '<i class="fa fa-star"></i> ';
                        }
                    } else {
                        $tempSts[] = '<i class="fa fa-star-o"></i> ';
                    }
                } else {
                    $tempSts[] = '<i class="fa fa-star-o"></i> ';
                }

                if ($anggota['tanggal_finger']) {
                    $tempSts[] = '<i class="fa fa-star"></i> ';
                } else {
                    $tempSts[] = '<i class="fa fa-star-o"></i> ';
                }

                if ($anggota['drop_manager']) {
                    $tempSts[] = '<i class="fa fa-star"></i> ';
                } else {
                    $tempSts[] = '<i class="fa fa-star-o"></i> ';
                }
            } else {
                $tempSts[] = 'MT / BC';
            }

            $aksi = [];
            $aksi[] = Html::a('Move SBC', null, [
                'class'=>'btn btn-warning btn-xs btn-modal-reset-manager',
                'data-id'=>$anggota['id'],
            ]);
            $aksi[] = Html::button("Hapus Akun", [
                'class'=>'btn btn-xs btn-danger btn-delete-account',
                'data-id'=>$anggota['id'],
            ]);

            $data[$key]['status'] = implode(' ', $tempSts);
            $data[$key]['aksi'] = implode(' ', $aksi);
        }

        return ['data'=>$data];
    }

    public function actionModalSetManager($id)
    {
        $model = new SetManagerForm;
        $model->id = $id;

        $managerList = Profil::find()
            ->joinWith('user')
            ->where(['user.user_role_id'=>3])
            ->asArray()
            ->all();

        if ($managerList) {
            $managerList = ArrayHelper::map($managerList, 'id', 'nama_lengkap');
        }

        return $this->renderAjax('_set_manager', get_defined_vars());
    }

    public function actionSetManager()
    {
        $post = Yii::$app->request->post();

        $model = new SetManagerForm;
        $model->load($post);
        if ($model->validate()) {
            $profil = Profil::findOne($model->id);
            $profil->manager_id = $model->manager_id;
            $profil->save();

            // Yii::$app->session->setFlash('success', "Pilihan SBC telah diubah.");
        }
    }

    public function actionDeleteProfil($id)
    {
        $profil = Profil::findOne($id);
        $profil->is_deleted = 1; // delete
        $profil->deleted_by = Yii::$app->user->identity->username; // delete
        $profil->deleted_dt = date('Y-m-d H:i:s'); // delete
        $profil->save();

        $user = UserModel::findOne($profil->user_id);

        $user->status = 0;
        $user->save();

        return true;
    }

}