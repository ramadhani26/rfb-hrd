<?php

namespace app\controllers;

use Yii;
use app\components\AmayaController;
use \app\models\Profil;
use \app\models\Soal;
use \app\models\JawabanSoal;
use \app\models\Lookup;
use yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class SesiSoalController extends AmayaController {
    
    public function actionIsi()
    {
        $profil = Profil::find()
            ->where(['user_id'=>Yii::$app->user->id])
            ->one();

        // validasi pernah isi soal
        $jawaban = JawabanSoal::find()
            ->where(['profil_id'=>$profil->id])
            ->one();
        if ($jawaban) {
            Yii::$app->session->setFlash('warning', "Profil ini sudah submit soal sebelumnya. Tidak diizinkan lagi untuk mengulang mengisi soal.");
            return $this->redirect('/profil/index');
        }


        $title = 'Sesi Pengisian Soal';

        $questions = Soal::find()->all();

        $choices = [];
        foreach ($questions as $key => $each) {
            if ($each->l_type && !isset($choices[$each->l_type])) {
                $lookupSoal = Lookup::find()
                    ->where(['type'=>$each['l_type']])
                    ->asArray()->all();
                $choices[$each->l_type] = ArrayHelper::map($lookupSoal, 'name', 'name');
            }
        }

        return $this->render('isi', get_defined_vars());
    }

    public function actionSubmitJawaban()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = Yii::$app->request->post();

        $questions = Soal::find()->all();

        // checking
        $validate = true;
        $errors = [];
        foreach ($questions as $key => $question) {
            if ($question->jenis == 'label') continue;
            if (!isset($post[$question->id]) || $post[$question->id] == '') {
                $errors[] = [
                    'id'=>$question->urutan,
                    'message'=>'Tidak Boleh Kosong'
                ];
                $validate = false;
            }
        }

        if (!$validate) {
            return [
                'status' => 422,
                'errors' => $errors, 
            ];
        }

        foreach ($questions as $key => $question) {
            if (!isset($post[$question->id])) continue;

            $jawaban = new JawabanSoal;
            $jawaban->profil_id = $post['profil_id'];
            $jawaban->soal = $question->pertanyaan;
            $jawaban->jawaban = is_array($post[$question->id]) ? implode(',', $post[$question->id]) : $post[$question->id];
            $jawaban->penentu = $question->penentu;

            if ($jawaban->save()) {
                $profil = Profil::findOne($post['profil_id']);

                $profil->has_soal_submitted = 1;
                $profil->save();
            }
        }
        return [
            'status' => 200,
            'errors' => []
        ];
    }

}
