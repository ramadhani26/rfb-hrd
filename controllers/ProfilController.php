<?php

namespace app\controllers;

use Yii;
use Yii\helpers\ArrayHelper;
use app\components\AmayaController;
use app\components\AmayaHelpers;
use \app\models\Profil;
use \app\models\ProfilAwalForm;
use \app\models\SetManagerForm;
use \app\models\Lookup;
use \app\models\Room;
use \app\models\Kota;
use yii\web\Response;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;

class ProfilController extends AmayaController
{
    public function actionCreate()
    {
        try 
        {
            $model = new ProfilAwalForm;

            $userRoleId = Yii::$app->user->identity->user_role_id;
            if ($userRoleId == 5) {
                $model->scenario = 'trainee';
            } elseif ($userRoleId == 3) {
                $model->scenario = 'manager';
            }
            $title = 'Tambah Profil';

            if (Yii::$app->request->isPost) {                
                $post = Yii::$app->request->post();
                $model->attributes = $post['ProfilAwalForm'];
                $model->nama_file_cv = UploadedFile::getInstance($model, 'nama_file_cv');
                $model->nama_file_ktp = UploadedFile::getInstance($model, 'nama_file_ktp');
                $model->nama_file_kk = UploadedFile::getInstance($model, 'nama_file_kk');
                $model->nama_file_ijazah = UploadedFile::getInstance($model, 'nama_file_ijazah');
                // $model->nama_file_skck = UploadedFile::getInstance($model, 'nama_file_skck');
                $model->nama_file_skck = "NULL";
                
                if ($model->posisi != 'Lainnya') {
                    // avoid validate
                    $model->posisi_lainnya = '-';
                }
                // var_dump($model);
                // die;
                if ($model->validate()) { 
                    // var_dump($model);
                    // die;
                    $profil = new Profil;
                    $profil->attributes = $model->attributes;
                    $profil->user_id = Yii::$app->user->id;
                    $profil->l_status_profil_id = 1;                    

                    if ($profil->posisi == 'Lainnya') {
                        $profil->posisi = $model->posisi_lainnya;
                    }

                    if ($profil->posisi == 'Lainnya') {
                        $profil->posisi = $model->posisi_lainnya;
                    }

                    $filenames = [];
                    // foreach ($profil->nama_file as $file) {
                    //     $filename = rand(100000, 999999) . '.' . $file->extension;
                    //     $file->saveAs('uploads/' . $filename);
                    //     $filenames[] = $filename;
                    // }
                    if ($userRoleId == 5) {
                        // var_dump($profil->nama_lengkap);
                        // $path = 'uploads/'. $profil->nama_lengkap;
                        // FileHelper::createDirectory($path);
                        // die;
                        $filenamecv = $model->nama_lengkap . '_cv.' . $model->nama_file_cv->extension;
                        $profil->nama_file = $model->nama_file_cv;
                        $profil->nama_file->saveAs('uploads/' . $filenamecv);
                        $filenames[] = $filenamecv;
                        $filenamektp = $model->nama_lengkap . '_ktp.' . $model->nama_file_ktp->extension;
                        $profil->nama_file = $model->nama_file_ktp;
                        $profil->nama_file->saveAs('uploads/' . $filenamektp);
                        $filenames[] = $filenamektp;
                        $filenamekk = $model->nama_lengkap . '_kk.' . $model->nama_file_kk->extension;
                        $profil->nama_file = $model->nama_file_kk;
                        $profil->nama_file->saveAs('uploads/' . $filenamekk);
                        $filenames[] = $filenamekk;
                        $filenameijazah = $model->nama_lengkap . '_ijazah.' . $model->nama_file_ijazah->extension;
                        $profil->nama_file = $model->nama_file_ijazah;
                        $profil->nama_file->saveAs('uploads/' . $filenameijazah);
                        $filenames[] = $filenameijazah;

                        // var_dump($profil->nama_lengkap);
                        // $path = 'uploads/'. $profil->nama_lengkap;
                        // FileHelper::createDirectory($path);
                        // // die;
                        // $filenamecv = $model->nama_lengkap . '_cv.' . $model->nama_file_cv->extension;
                        // $profil->nama_file = $model->nama_file_cv;
                        // $profil->nama_file->saveAs('uploads/' . $profil->nama_lengkap . '/' . $filenamecv);
                        // $filenames[] = $filenamecv;
                        // $filenamektp = $model->nama_lengkap . '_ktp.' . $model->nama_file_ktp->extension;
                        // $profil->nama_file = $model->nama_file_ktp;
                        // $profil->nama_file->saveAs('uploads/' . $profil->nama_lengkap . '/' . $filenamektp);
                        // $filenames[] = $filenamektp;
                        // $filenamekk = $model->nama_lengkap . '_kk.' . $model->nama_file_kk->extension;
                        // $profil->nama_file = $model->nama_file_kk;
                        // $profil->nama_file->saveAs('uploads/' . $profil->nama_lengkap . '/' . $filenamekk);
                        // $filenames[] = $filenamekk;
                        // $filenameijazah = $model->nama_lengkap . '_ijazah.' . $model->nama_file_ijazah->extension;
                        // $profil->nama_file = $model->nama_file_ijazah;
                        // $profil->nama_file->saveAs('uploads/' . $profil->nama_lengkap . '/' . $filenameijazah);

                        // $filenameskck = $model->nama_lengkap . '_skck.' . $model->nama_file_skck->extension;
                        // $profil->nama_file = $model->nama_file_skck;
                        // $profil->nama_file->saveAs('uploads/' . $filenameskck);
                        // $filenames[] = $filenameskck;
                    }

                    $profil->nama_file = implode(',', $filenames);

                    $profil->tanggal_lahir = date('Y-m-d', strtotime($profil->tanggal_lahir));

                    if ($profil->save()) {
                        Yii::$app->session->setFlash('success', "Profil tersimpan.");
                        return $this->redirect('index');
                    }else{
                        Yii::$app->session->setFlash('error', "Gagal Tersimpan.");
                    }
                }
                else{
                    // var_dump($model->validate());
                    // die;
                    Yii::$app->session->setFlash('error', "Isi Semua Kolom-kolom yang di ada di menu tambah Profil.");
                }
            }

            $lookupPosisi = Lookup::find()
                ->where(['type'=>'posisi'])
                ->asArray()->all();
            $rooms = Room::find()->asArray()->all();

            $listPosisi = ArrayHelper::map($lookupPosisi, 'name', 'name');
            $listPosisi['Lainnya'] = 'Lainnya';
            $listJenisKelamin = ['Laki-laki'=>'Laki-laki', 'Perempuan'=>'Perempuan'];
            $listRoom = ArrayHelper::map($rooms, 'id', 'nama_room');
            
            return $this->render('create', get_defined_vars());
        } 
        catch (ErrorException $e) 
        {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }        
    }

    public function actionModalSetManager($id)
    {
        $model = new SetManagerForm;
        $model->id = $id;

        $managerList = Profil::find()
            ->joinWith('user')
            ->where(['user.user_role_id'=>3])
            ->asArray()
            ->all();

        if ($managerList) {
            $managerList = ArrayHelper::map($managerList, 'id', 'nama_lengkap');
        }

        return $this->renderAjax('_set_manager', get_defined_vars());
    }



    public function actionSetManager()
    {
        $post = Yii::$app->request->post();

        $model = new SetManagerForm;
        $model->load($post);
        if ($model->validate()) {
            $profil = Profil::findOne($model->id);
            $profil->manager_id = $model->manager_id;
            $profil->save();

            Yii::$app->session->setFlash('success', "Pilihan SBC telah tersimpan, Tunggu approve dari Team SBC untuk tahap selanjutnya.");
        }
    }

    public function actionIndex()
    {
        $title = 'Profil Pengguna';
        $profil = Profil::find()
            ->where(['user_id'=>Yii::$app->user->id])
            ->one();
        if (!$profil) {
            $username = Yii::$app->user->identity->username;
            Yii::$app->session->setFlash('danger', "User {$username} belum membuat profil sebelumnya. Silakan isi profil terlebih dahulu.");
            return $this->redirect('/profil/create');
        }

        $followers = $followerTrainees = $followerActives = $managers = $coWorkers = [];
        if ($profil->user->user_role_id == 3) { // manager
            $followers = Profil::find()
                ->joinWith('user')
                ->where([
                    // 'user.user_role_id'=>5,
                    'manager_id'=>$profil->id,
                    'manager_approved'=>1,
                    'm_profil.is_deleted'=>0,
                ])
                ->all();

            foreach ($followers as $key => $follower) {
                // var_dump($follower->user->user_role_id);
                if(($follower != null)&&($follower instanceof SysMessage)){
                    if ($follower->user->user_role_id == 5) {
                        $followerTrainees[] = $follower;
                    } elseif ($follower->user->user_role_id == 6) {
                        $followerActives[] = $follower;
                    }
                }
                // die;                
            }
            // die;
            return $this->render('index_manager', get_defined_vars());
        } elseif ($profil->user->user_role_id == 4) { // HRD
            $managers = Profil::find()
                ->joinWith('user')
                ->where([
                    'user.user_role_id'=>3,
                    'm_profil.is_deleted'=>0
                ])
                ->all();

            foreach ($managers as $manager) {
                $trainees_ = Profil::find()
                    ->joinWith('user')
                    ->where([
                        'user.user_role_id'=>5,
                        'manager_id'=>$manager->id,
                        'manager_approved'=>1,
                        'm_profil.is_deleted'=>0,
                    ])->count();
                $followerTrainees[$manager->id] = $trainees_;

                $actives_ = Profil::find()
                    ->joinWith('user')
                    ->where([
                        'user.user_role_id'=>6,
                        'manager_id'=>$manager->id,
                        'manager_approved'=>1,
                        'm_profil.is_deleted'=>0,
                    ])
                    // ->andWhere(['!=', 'user.user_role_id', 5])
                    ->count();
                $followerActives[$manager->id] = $actives_;
            }


            return $this->render('index_hrd', get_defined_vars());
        } elseif (in_array($profil->user->user_role_id, [5, 6])) {
            // $coWorkers = Profil::find()
            //     ->where(['manager_id'=>$profil->manager_id])
            //     ->andWhere(['!=', 'id', $profil->id])
            //     ->all();
            return $this->render('index', get_defined_vars());
        }


    }

    public function actionGetProfil($id)
    {
        // Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Profil::findOne($id);

        // return $model->attributes;

        return $this->renderAjax('_get_profil', get_defined_vars());
    }

    public function actionDownloadDokumen($id)
    {
        // echo "<pre>";
        // print_r(Url::base(true) . '/uploads/946710.jpg');
        // exit();
        $model = Profil::findOne($id);

        if ($model && $model->nama_file) {
            $filenames = explode(',', $model->nama_file);
                // foreach ($filenames as $key => $filename) {
                //     print_r(Yii::getAlias('@app') . "\uploads\\" . $filename);
                // }
                // die();

            $zip = new \ZipArchive();
            if ($zip->open('uploads/' . $model->nama_lengkap . '.zip',\ZipArchive::CREATE | \ZipArchive::OVERWRITE) === TRUE) {
                foreach ($filenames as $key => $filename) {
                    $zip->addFile('uploads/' . $filename);
                }
                $zip->close();
            }

            if (file_exists('uploads/' . $model->nama_lengkap . '.zip')) {
               Yii::$app->response->sendFile('uploads/' . $model->nama_lengkap . '.zip');
            // unlink('uploads/' . $model->nama_lengkap . '.zip');
            }
        }
    }


    public function actionRequestRecall($id)
    {
        $profil = Profil::findOne($id);
        $profil->request_to_recall = 1; // requested

        return $profil->save();
    }

    public function actionGetUmur($tgl_lahir)
    {
        return AmayaHelpers::getUmur($tgl_lahir);
    }

    public function actionGetKota($q = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $results = [];
        $q = strtolower($q);
        $kota = Kota::find()
            ->select(['nama as id', 'nama as text'])
            ->andWhere(['like', 'LOWER(nama)', $q])
            ->asArray()->all();



        return ['results' => $kota];
    }
}
