<?php

namespace app\controllers;

use Yii;
use app\components\AmayaController;
use app\components\AmayaHelpers;
use \app\models\Profil;
use \app\models\Room;
use \app\models\JawabanSoal;
use yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use yii2mod\user\models\UserModel;

class DashboardController extends AmayaController {

    public $user;
    public function init() {
        if (Yii::$app->user->isGuest) return ;
        $this->user = Yii::$app->user->identity;
        if ($this->user->user_role_id != 4) {
            return $this->redirect('/site/403');
        }
    }

    public function actionIndex()
    {
        $title = 'Dashboard';

        // search content
        $sbc = Profil::find()
            ->joinWith('user')
            ->where([
                'user.user_role_id'=>3,
                'm_profil.is_deleted'=>0
            ])
            ->orderBy(['m_profil.nama_lengkap'=>SORT_ASC])
            ->asArray()->all();
        $sbcList = ArrayHelper::map($sbc, 'id', 'nama_lengkap');
        $room = Room::find()->asArray()->all();
        $roomList = ArrayHelper::map($room, 'id', 'nama_room');

        $request = Yii::$app->request;
        $getFrom = $request->get('from', null);
        $getTo = $request->get('to', null);
        $getRoomId = $request->get('room', null);
        $getSbcId = $request->get('sbc', null);
        $filterForm = ($getFrom ? date('d-m-Y', strtotime($getFrom)) : date('d-m-Y', strtotime('-1 month')));
        $filterForm .= ' s.d ';
        $filterForm .= ($getTo ? date('d-m-Y', strtotime($getTo)) : date('d-m-Y'));

        $from = ($getFrom ? date('Y-m-d', strtotime($getFrom)) : date('Y-m-d', strtotime('-1 month'))) . ' 00:00:00';
        $to = ($getTo ? date('Y-m-d', strtotime($getTo)) : date('Y-m-d')) . ' 23:59:59';



        $managers = Profil::find()
            ->joinWith('user')
            ->where([
                'user.user_role_id'=>3,
                'm_profil.is_deleted'=>0
            ]);
        if ($getSbcId) {
            $managers = $managers->andWhere(['m_profil.id'=>$getSbcId]);
        }
        if ($getRoomId) {
            $managers = $managers->andWhere(['m_profil.manager_room_id'=>$getRoomId]);
        }
        $managers = $managers->orderBy(['m_profil.nama_lengkap'=>SORT_ASC]);
        // echo "<pre>";
        // print_r($managers->createCommand()->getRawSql());
        // exit();
        $managers = $managers->asArray()->all();

        $cntTrainee = $cntStaff = $arrManager = [];
        foreach ($managers as $key => $manager) {
            $arrManager[] = $manager['nama_lengkap'];
            $cntTrainee[] = Profil::find()
                ->joinWith('user')
                ->where([
                    'm_profil.manager_id' => $manager['id'],
                    'm_profil.is_deleted' => 0,
                    'user.user_role_id' => 5,
                ])
                ->andWhere(['between', 'created_dt', $from, $to])
                ->count();
            $cntStaff[] = Profil::find()
                ->joinWith('user')
                ->where([
                    'm_profil.manager_id' => $manager['id'],
                    'm_profil.is_deleted' => 0,
                    'user.user_role_id' => 6,
                ])
                ->andWhere(['between', 'created_dt', $from, $to])
                ->count();
        }


        return $this->render('index', get_defined_vars());
    }
}