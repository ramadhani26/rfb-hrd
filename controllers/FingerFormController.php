<?php

namespace app\controllers;

use Yii;
use Yii\helpers\ArrayHelper;
use app\components\AmayaController;
use \app\models\Profil;
use \app\models\Provinsi;
use \app\models\Kota;
use \app\models\Kecamatan;
use \app\models\Kelurahan;
use \app\models\FingerForm;
use \app\models\Lookup;
use \app\models\Office;
use yii\web\Response;
use yii\helpers\Url;

class FingerFormController extends AmayaController
{
    
    public function actionCreate($id)
    {
        $title = "Form Pengisian Biodata Fingerprint";
        $profil = Profil::findOne($id);


        $model = new FingerForm;
        $model->attributes = $profil->attributes;
        // $model->tanggal_berlaku = date('Y-m-d', strtotime($model->tanggal_berlaku));
        // $model->tanggal_lahir = date('Y-m-d', strtotime($model->tanggal_lahir));
        $model->profil_id = $id;
        $model->email = Yii::$app->user->identity->email;

        if ($model->load(Yii::$app->request->post())) {
            $model->tanggal_berlaku = date('Y-m-d', strtotime($model->tanggal_berlaku));
            $model->tanggal_lahir = date('Y-m-d', strtotime($model->tanggal_lahir));
            if ($model->validate()) {
                $profil->attributes = $model->attributes;
                $profil->tanggal_finger = $profil->tanggal_finger ? : date('Y-m-d');
                $profil->update();
                Yii::$app->session->setFlash('success', "Form Finger berhasil disimpan.");
                return $this->redirect('/profil/index');
            }
        }

        $listJenisId = ['KTP'=>'KTP', 'SIM'=>'SIM', 'Paspor'=>'Paspor', 'Kitas'=>'Kitas'];
        $listAgama = ['Islam'=>'Islam', 'Kristen'=>'Kristen', 'Hindu'=>'Hindu', 'Budha'=>'Budha', 'Kong Hu Cu'=>'Kong Hu Cu', 'Lainnya'=>'Lainnya'];
        $listJenisKelamin = ['Laki-laki'=>'Laki-laki', 'Perempuan'=>'Perempuan'];
        $listPendidikanTerakhir = ['SD'=>'SD', 'SMP'=>'SMP', 'SMA'=>'SMA', 'D1'=>'D1', 'D3'=>'D3', 'S1'=>'S1', 'S2'=>'S2', 'S3'=>'S3'];
        $listStatusPerkawinan = ['Belum Menikah'=>'Belum Menikah', 'Menikah'=>'Menikah', 'Janda / Duda'=>'Janda / Duda'];
        $listOffice = ArrayHelper::map(Office::find()->asArray()->all(), 'nama_kantor', 'nama_kantor');
        $listProvinsi = Provinsi::find()->asArray()->all();

        return $this->render('create', get_defined_vars());
    }

    public function actionListKota() {
        $request = Yii::$app->request;
        $post = $request->post();
        if($post['depdrop_parents'][0] && $post['depdrop_parents'][0] != 'Loading ...') {
            $out = [];
            $selected = null;
            $provinsi = Provinsi::find()
                ->where(['nama'=>$post['depdrop_parents'][0]])->one();
            $out = Kota::find()
                ->select(['nama as id', 'nama as name'])
                ->where(['provinsi_id'=>$provinsi->id])
                ->asArray()->all();

            echo json_encode(['output'=>$out, 'selected'=>$selected]);
            return;
        }
        echo json_encode(['output'=>'', 'selected'=>'']);
    }
    public function actionListKecamatan() {
        $request = Yii::$app->request;
        $post = $request->post();
        if($post['depdrop_parents'][0] && $post['depdrop_parents'][0] != 'Loading ...') {
            $out = [];
            $selected = null;
            $kota = Kota::find()
                ->where(['nama'=>$post['depdrop_parents'][0]])->one();
            $out = Kecamatan::find()
                ->select(['nama as id', 'nama as name'])
                ->where(['kota_id'=>$kota->id])
                ->asArray()->all();

            echo json_encode(['output'=>$out, 'selected'=>$selected]);
            return;
        }
        echo json_encode(['output'=>'', 'selected'=>'']);
    }
    public function actionListKelurahan() {
        $request = Yii::$app->request;
        $post = $request->post();
        if($post['depdrop_parents'][0] && $post['depdrop_parents'][0] != 'Loading ...') {
            $out = [];
            $selected = null;
            $kecamatan = Kecamatan::find()
                ->where(['nama'=>$post['depdrop_parents'][0]])->one();
            $out = Kelurahan::find()
                ->select(['nama as id', 'nama as name'])
                ->where(['kecamatan_id'=>$kecamatan->id])
                ->asArray()->all();

            echo json_encode(['output'=>$out, 'selected'=>$selected]);
            return;
        }
        echo json_encode(['output'=>'', 'selected'=>'']);
    }
}