<?php

namespace app\controllers;

use Yii;
use app\components\AmayaController;
use app\components\AmayaHelpers;
use yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use \app\models\Room;
use \app\models\Profil;

class InfoRoomController extends AmayaController {

    public $user;
    public function init() {
        if (Yii::$app->user->isGuest) return ;
        $this->user = Yii::$app->user->identity;
        if ($this->user->user_role_id != 4) {
            return $this->redirect('/site/403');
        }
    }


    public function actionIndex()
    {
        $title = 'Informasi Room';

        return $this->render('index', get_defined_vars());
    }

    public function actionGetData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $rooms = Room::find()->asArray()->all();
        $data = [];

        foreach ($rooms as $key=>$room) {
            $data[$key] = $room;

            // list_manager
            $temp = [];
            $managers = Profil::find()
                ->where([
                    'manager_room_id'=>$room['id'],
                    'is_deleted'=>0,
                ])
                ->asArray()->all();
            $list_manager = ArrayHelper::map($managers, 'id', 'nama_lengkap');
            foreach ($list_manager as $each) {
                $temp[] = '- ' . $each . '<br>';
            }
            $data[$key]['list_manager'] = implode('', $temp);

            // aksi
            $aksi = [];
            $aksi[] = Html::button("Tampilkan Soal", [
                'class'=>'btn btn-xs btn-warning btn-blast-soal',
                'data-id'=>$room['id'],
            ]);
            $data[$key]['aksi'] = implode(' ', $aksi);
        }

        return ['data'=>$data];
    }

    public function actionShowSoal($id) 
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $room = Room::findOne($id);
        $sql = "
            SELECT trainee.*
            FROM m_profil trainee
                JOIN m_profil manager ON trainee.manager_id = manager.id AND trainee.manager_approved = 1
            WHERE 
                manager.manager_room_id = {$id}
                AND trainee.is_deleted = 0
                AND trainee.has_soal_showed = 0;
        ";
        $listTraineePerRoom = Yii::$app->db->createCommand($sql)->queryAll();


        foreach ($listTraineePerRoom as $key => $traineePerRoom) {
            $profil = Profil::findOne($traineePerRoom['id']);

            $profil->has_soal_showed = 1;
            $profil->update();
        }
        // $profil->response_recall = 1; // approve

        // return $profil->save();
        return ['room'=>$room];
    }
}