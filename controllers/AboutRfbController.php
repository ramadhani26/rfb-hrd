<?php

namespace app\controllers;

use Yii;
use app\components\AmayaController;
use app\components\AmayaHelpers;
use yii\web\Response;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use yii2mod\user\models\UserModel;

class AboutRfbController extends AmayaController {

    public $user;
    public function init() {
        if (Yii::$app->user->isGuest) return ;
        // $this->user = Yii::$app->user->identity;
        // if ($this->user->user_role_id != 4) {
        //     return $this->redirect('/site/403');
        // }
    }

    public function actionIndex()
    {
        $title = 'About RFB Bandung';
        return $this->render('index', compact('title'));
    }
}