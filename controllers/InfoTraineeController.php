<?php

namespace app\controllers;

use Yii;
use app\components\AmayaController;
use app\components\AmayaHelpers;
use \app\models\Profil;
use \app\models\JawabanSoal;
use yii\web\Response;
use yii\helpers\Html;
use yii\data\ArrayDataProvider;
use yii2mod\user\models\UserModel;

use PhpOffice\PhpSpreadsheet\Spreadsheet; 
use PhpOffice\PhpSpreadsheet\Writer\Xlsx; 

use yii\base\ErrorException;

class InfoTraineeController extends AmayaController {
    public $user;
    public function init() {
        if (Yii::$app->user->isGuest) return ;
        $this->user = Yii::$app->user->identity;
        if (!in_array($this->user->user_role_id, [3, 4])) {
            return $this->redirect('/site/403');
        }
    }

    public function actionIndex()
    {
        $title = 'Informasi Trainee';

        return $this->render('index', get_defined_vars());
    }

    public function actionGetData()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $is_manager = Yii::$app->user->identity->user_role_id == 3 ? true : false;


        $model = Profil::find()
            ->joinWith(['lookupStatusProfil', 'user'])
            ->where(['user_role_id'=>5, 'm_profil.is_deleted'=>0]);
        if ($is_manager) {
            $manager = Profil::find()
                ->where(['user_id'=>Yii::$app->user->id])
                ->one();
            $model->andWhere(['manager_id'=>$manager->id]);
        }
        $model = $model->orderBy(['id'=>SORT_DESC])
            ->asArray()->all();

        $data = [];
        foreach ($model as $key => $each) {// action button - start
            $aksi = [];
            if ($each['nama_file']) {
                $aksi[] = Html::a('Download Dokumen', '/profil/download-dokumen?id=' . $each['id'], ['class'=>'btn btn-success btn-xs']);
            }

            if ($each['l_status_profil_id'] == 1 && $is_manager == false) {
                $aksi[] = Html::button('Aktifkan', [
                    'class'=>'btn btn-xs btn-info btn-aktivasi',
                    'data-id'=>$each['id'],
                ]);
            } elseif ($each['l_status_profil_id'] == 2) {
                if ($each['manager_id']) {
                    if ($each['manager_approved'] == 0 && $is_manager == true) {
                        $aksi[] = Html::button('Approve SBC', [
                            'class'=>'btn btn-xs btn-info btn-approve-manager',
                            'data-id'=>$each['id'],
                            'data-managerId'=>$each['manager_id'],
                        ]);
                    } else {
                        if ($each['request_to_recall'] == 1 && $is_manager == false) {
                            if ($each['response_recall'] == 0) {
                                $aksi[] = Html::button('Approve Recall', [
                                    'class'=>'btn btn-xs btn-info btn-approve-recall',
                                    'data-id'=>$each['id'],
                                    'data-managerId'=>$each['manager_id'],
                                ]);
                            }
                        }

                        if ($each['tanggal_finger']) {
                            if (!$each['drop_manager']) {
                                $aksi[] = Html::button('Drop SBC', [
                                    'class'=>'btn btn-xs btn-info btn-drop-manager',
                                    'data-id'=>$each['id'],
                                    'data-managerId'=>$each['manager_id'],
                                ]);
                                $aksi[] = '<button type="button" class="btn btn-xs btn-info btn-drop-manager" data-id="'.$each['id'].'" data-managerId="'.$each['manager_id'].'">Drop SBC</button>';
                            } else {
                                if ($is_manager == false) {
                                    $aksi[] = Html::button('Lolos Trainee', [
                                        'class'=>'btn btn-xs btn-info btn-lolos-trainee',
                                        'data-id'=>$each['id'],
                                        'data-managerId'=>$each['manager_id'],
                                    ]);
                                } 
                            }
                        }
                    }
                }
            }

            $data[$key] = $each;

            $data[$key]['nama_lengkap'] = Html::button($each['nama_lengkap'], ['class'=>'btn btn-link btn-profile-detail', 'data-id'=>$each['id']]);
            $btn = implode('<br>', $aksi);
            // $btn = '<div id="content-"'. $each['id'] . '"></div>';
            $data[$key]['nama_lengkap'] .= "<i class='fa fa-info' data-toggle='popover' data-container='body' data-trigger='click' data-html=true data-placement='right' data-content='".$btn."'></i>";

            $manager = Profil::findOne($each['manager_id']);
            if ($manager) {
                $data[$key]['manager'] = $manager ? $manager->nama_lengkap : '';
                if ($manager->is_deleted == 1) {
                    $data[$key]['manager'] .= ' (DELETED)';
                }
                $data[$key]['room'] = $manager->managerRoom->nama_room;
            } else {
                $data[$key]['manager'] = '';
                $data[$key]['room'] = '';
            }

            $data[$key]['ttl'] = $each['tempat_lahir'] . ', ' . date('d M Y', strtotime($each['tanggal_lahir']));
            $data[$key]['umur'] = AmayaHelpers::getUmur($each['tanggal_lahir']);

            if ($each['l_status_profil_id'] == 1) {
                $statusName = $each['lookupStatusProfil']['name'];
                $badge = "bg-red";
                $message = $statusName; 
            } else {
                if ($each['manager_id']) {
                    if ($each['manager_approved'] == 0) {
                        $badge = "bg-orange";
                        $message = 'Request SBC';
                    } else {
                        $badge = "bg-blue";
                        $message = 'Aktif Trainee';
                    }
                } else {
                    $badge = "bg-orange";
                    $message = 'Belum pilih SBC';
                }
            }
            $status = "<span class='badge {$badge}'>{$message}</span>";
            $data[$key]['status'] = $status;

            $badgeSoal = 'bg-orange';
            $result_soal = '';
            if (!$each['has_soal_submitted']) {
                $badgeSoal = 'bg-white';
                $result_soal = 'Belum isi soal';
            } else {
                if ($each['has_soal_approved'] == 0) {
                    $badgeSoal = 'bg-orange';
                    $result_soal = 'Menunggu Hasil Nilai';
                } else {
                    if ($each['has_soal_approved'] == 1 || ($each['has_soal_approved'] == 2 && $each['response_recall'] == 1)) {
                        $badgeSoal = 'bg-green';
                        $result_soal = 'Berhasil';
                    } else {
                        if ($each['request_to_recall'] == 0) {
                            $badgeSoal = 'bg-red';
                            $result_soal = 'Failed & Waiting Recall';
                        } elseif ($each['request_to_recall'] == 1) {
                            $badgeSoal = 'bg-orange';
                            $result_soal = 'Waiting Approve Recall';
                        }
                    }
                }
            }
            $data[$key]['status_soal'] = "<span class='badge {$badgeSoal}'>{$result_soal}</span>";

            $badgeFinger = 'bg-orange';
            $resultFinger = '';
            if (!$each['tanggal_finger']) {
                $badgeFinger = 'bg-orange';
                $resultFinger = 'Belum Isi';
            } else {
                $badgeFinger = 'bg-green';
                $resultFinger = 'Sudah Isi';
            }
            $data[$key]['status_finger'] = "<span class='badge {$badgeFinger}'>{$resultFinger}</span>";

            $badgeDropMan = 'bg-orange';
            $resultDropMan = '';
            if (!$each['drop_manager']) {
                $badgeDropMan = 'bg-orange';
                $resultDropMan = 'Belum Drop';
            } else {
                $badgeDropMan = 'bg-green';
                $resultDropMan = 'Sudah Drop';
            }
            $data[$key]['drop_manager'] = "<span class='badge {$badgeDropMan}'>{$resultDropMan}</span>";


            

            $data[$key]['aksi'] = implode(' ', $aksi);
            // action button - end

            // remove account button - start
            $akun = [];
            if ($is_manager == false) {
                # code...
                if ($each['manager_id']) {
                    $akun[] = Html::a('Move SBC', null, [
                        'class'=>'btn btn-warning btn-xs btn-modal-reset-manager',
                        'data-id'=>$each['id'],
                    ]);
                }
                $akun[] = Html::button("Hapus Akun", [
                    'class'=>'btn btn-xs btn-danger btn-delete-account',
                    'data-id'=>$each['id'],
                ]);
            } else {
                $akun[] = 'not allowed';
            }
            $data[$key]['akun'] = implode(' ', $akun);
            // remove account button - end
        }

        return ['data'=>$data];
    }

    public function actionDownloadExcel()
    {
        if ($this->user->user_role_id != 4) return $this->redirect('/site/403');

        // $is_manager = Yii::$app->user->identity->user_role_id == 3 ? true : false;
        $model = Profil::find()
            ->joinWith(['lookupStatusProfil', 'user'])
            ->where(['user_role_id'=>5, 'm_profil.is_deleted'=>0])
            ->andWhere(['IS NOT', 'm_profil.tanggal_finger', null]);

        $model = $model->all();

        $no = 0;
        $data = [];
        foreach ($model as $key => $trainee) {
            $data[$key] = $trainee->attributes;
            $data[$key]['no'] = ++$no;
            $data[$key]['nama_karyawan'] = $trainee->nama_lengkap;
            $data[$key]['gender'] = $trainee->jenis_kelamin;
            $data[$key]['perlahiran'] = $trainee->tempat_lahir . ', ' . date('d M Y', strtotime($trainee->tanggal_lahir)) . " / " . AmayaHelpers::getUmur($trainee->tanggal_lahir);
            $data[$key]['joined_date'] = $trainee->tanggal_finger ? date('d M Y', strtotime($trainee->tanggal_finger)) : '';
            $data[$key]['jabatan'] = 'Business Consultant';
            $data[$key]['agama'] = $trainee->agama;
            $data[$key]['no_identitas'] = $trainee->nomor_identitas . ' (' . $trainee->jenis_identitas . ')';
            $data[$key]['tanggal_berlaku'] = date('d M Y', strtotime($trainee->tanggal_berlaku));
            $alamat_identitas = [];
            $alamat_identitas[] = $trainee->alamat_identitas ? : '';
            $alamat_identitas[] = $trainee->rt_identitas ? ' RT ' . $trainee->rt_identitas : '';
            $alamat_identitas[] = $trainee->rw_identitas ? ' RW ' . $trainee->rw_identitas : '';
            $alamat_identitas[] = $trainee->kelurahan_identitas ? ' Kelurahan ' . $trainee->kelurahan_identitas : '';
            $alamat_identitas[] = $trainee->kecamatan_identitas ? ' Kecamatan ' . $trainee->kecamatan_identitas : '';
            $alamat_identitas[] = $trainee->kota_identitas ? ', ' . $trainee->kota_identitas : '';
            $alamat_identitas[] = $trainee->provinsi_identitas ? ', ' . $trainee->provinsi_identitas : '';
            $data[$key]['alamat_identitas'] = implode('', $alamat_identitas);
            $data[$key]['pendidikan_terakhir'] = $trainee->pendidikan_terakhir;
            $data[$key]['tipe_rekrutmen'] = $trainee->jawabanSoalSumberLowongan ? $trainee->jawabanSoalSumberLowongan->jawaban : '';
            $data[$key]['status_perkawinan'] = $trainee->status_perkawinan;
            $data[$key]['manager'] = $trainee->manager ? $trainee->manager->nama_lengkap : '';
            $data[$key]['ibu_kandung'] = $trainee->ibu_kandung;
            $data[$key]['no_handphone'] = $trainee->no_handphone;
            $data[$key]['email'] = $trainee->email;
        }

        $headers = [
            'no' => 'No',
            'nama_karyawan' => 'Nama Karyawan',
            'gender' => 'Sex',
            'perlahiran' => 'Date of Birth', // ttl, umur
            'joined_date' =>'Joined Date', // dihitung dari isi fingerprint
            'jabatan' => 'Jabatan', // kosongin
            'agama' =>'Agama',
            'no_identitas' => 'No.KTP',
            'tanggal_berlaku' => 'Tgl.Berlaku KTP',
            'alamat_identitas' => 'Alamat Sesuai KTP', // Alamat RT RW Kelurahan Kecamatan Kab/Kota Provinsi
            'pendidikan_terakhir' => 'Educational background', // pendidikan terakhir
            'tipe_rekrutmen' => 'Recuitmen Type', // kosongin
            'status_perkawinan' => 'Marital Status', 
            'manager' => 'Upline', // nama manager
            'ibu_kandung' => 'Ibu Kandung',
            'no_handphone' => 'No hp',
            'email' => 'Alamat Email'
        ];

        // \moonland\phpexcel\Excel::export([
        //     'models' => $provider->getModels(),
        //     'columns' => array_keys($headers),
        //     'headers' => $headers, 
        //     'fileName' => 'Peserta Pelatihan Perdagangan Berjangka ' . date('d-m-Y'),
        //     'format' => 'Xlsx',
        //     'asAttachment' => true,
        // ]);


        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        //header
        $colstart = 1;
        $row = 1;
        foreach ($headers as $key => $header) {
            $col = chr(64+$colstart);
            $sheet->setCellValue($col.$row, $header);
            $styleArray = [
                'font' => [
                    'bold' => true,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                    'wrapText' => true,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'bottom' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];
            $sheet->getStyle($col.$row)->applyFromArray($styleArray);
            if (!in_array($col, ['E', 'F', 'L', 'N'])) {
                $sheet->getStyle($col.$row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFEA9999');
            } else {
                $sheet->getStyle($col.$row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
            }

            $sheet->getRowDimension($row)->setRowHeight(50);
            $sheet->getColumnDimension($col)->setWidth(15);
            $colstart++;
        }

        //data
        $row = 2;
        foreach ($data as $key => $datum) {
            $colstart = 1;
            foreach ($headers as $k => $header) {
                $col = chr(64+$colstart);
                $styleArray = [
                    'borders' => [
                        'top' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'bottom' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'left' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                        'right' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                        ],
                    ],
                ];
                $sheet->setCellValue($col.$row, $data[$key][$k]);
                $sheet->getStyle($col.$row)->applyFromArray($styleArray);
                $colstart++;
            }
            $row++;
        }


        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $filename = 'Peserta Pelatihan Perdagangan Berjangka ' . date('d-m-Y') . '.xlsx';
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");

    }

    public function actionAktivasi($id)
    {
        if ($this->user->user_role_id != 4) return $this->redirect('/site/403');

        $profil = Profil::findOne($id);
        $profil->l_status_profil_id = 2; // aktif

        return $profil->save();
    }

    public function actionApproveManager($id, $manager_id)
    {
        $profil = Profil::findOne($id);
        $profil->manager_approved = 1; // approve

        if ($profil->save()) {
            // send emaill confirm hari rabu
            setlocale(LC_ALL, 'id_ID');
            $dayNow = date('N');
            $dayNow = $dayNow > 3 ? $dayNow - 7 : $dayNow;
            $tues = 3 - $dayNow;

            Yii::$app->session->setFlash('success', "Trainee Sudah di Approve.");
            return $this->redirect('info-trainee');

            // send email konfirmasi berhasil
            try
            {
                $date = strftime('%A, %d %B %Y', strtotime('+' . $tues . ' day'));
                Yii::$app->mailer->compose('/info-trainee/_mail_content_rabu', ['date' => $date])
                ->setFrom(Yii::$app->params['senderEmail'])
                ->setTo($profil->user->email)
                ->setSubject('Friendly Reminder')
                ->send();
            }
            catch(ErrorException $e)
            {                
                Yii::$app->session->setFlash('error', $e->getMessage());
            }            
        } else {
            Yii::$app->session->setFlash('error', "Trainee Gagal untuk di Approve.");
            return false;
        }
    }

    public function actionApproveRecall($id)
    {
        if ($this->user->user_role_id != 4) return $this->redirect('/site/403');

        $profil = Profil::findOne($id);
        $profil->response_recall = 1; // approve

        return $profil->save();
    }

    public function actionDropManager($id)
    {
        $profil = Profil::findOne($id);
        $profil->drop_manager = 1; // drop approve

        return $profil->save();
    }

    public function actionFinishTest($id)
    {
        if ($this->user->user_role_id != 4) return $this->redirect('/site/403');

        $profil = Profil::findOne($id);
        $profil->is_lolos = 1;
        $profil->save();

        $user = $profil->user;
        $user->user_role_id = 6; // MT / BC
        return $user->save();
    }

    public function actionDeleteProfil($id)
    {
        if ($this->user->user_role_id != 4) return $this->redirect('/site/403');

        $profil = Profil::findOne($id);
        $profil->is_deleted = 1; // delete
        $profil->deleted_by = Yii::$app->user->identity->username; // delete
        $profil->deleted_dt = date('Y-m-d H:i:s'); // delete
        $profil->save();

        $user = UserModel::findOne($profil->user_id);

        $user->status = 0;
        $user->save();

        return true;
    }

    public function actionGenerateHasilSoal()
    {
        setlocale(LC_ALL, 'id_ID');
        // $x = Yii::$app->mailer->compose('/info-trainee/_mail_content')
        //     ->setFrom(Yii::$app->params['senderEmail'])
        //     ->setTo('rindeandra@gmail.com')
        //     ->setSubject('Friendly Reminder')
        //     ->send();

        //     return $x;

        if ($this->user->user_role_id != 4) return $this->redirect('/site/403');

        Yii::$app->response->format = Response::FORMAT_JSON;
        $profil = Profil::find()
            ->where(['has_soal_submitted'=>1, 'has_soal_approved'=>0])
            ->all();

        $successList = $failedList = [];

        $dayNow = date('N');
        $dayNow = $dayNow > 4 ? $dayNow - 7 : $dayNow;
        $tues = 4 - $dayNow;
        
        foreach ($profil as $key => $each) {
            $jawaban = JawabanSoal::find()
                ->where([
                    'profil_id'=>$each->id,
                    'penentu'=>1
                ])
                ->asArray()
                ->one();

            $arrPilihan = explode(',', $jawaban['jawaban']);

            if (in_array($each->posisi, ['Business Consultant', 'Manager Trainee']) ||in_array('Bussiness Consultant', $arrPilihan) || in_array('MT', $arrPilihan)) {
                $each->has_soal_approved = 1; // berhasil
                $each->save();
                $successList[] = $each->nama_lengkap . ' : ' . "Berhasil.";

                // send email konfirmasi berhasil
                $date = strftime('%A, %d %B %Y', strtotime('+' . $tues . ' day'));
                Yii::$app->mailer->compose('/info-trainee/_mail_content_kamis', ['date' => $date])
                    ->setFrom(Yii::$app->params['senderEmail'])
                    ->setTo($each->user->email)
                    ->setSubject('Pengumuman Lolos Test')
                    ->send();
            } else {
                $each->has_soal_approved = 2; // gagal
                $each->save();
                $failedList[] = $each->nama_lengkap . ' : ' . "Gagal.\n";
            }
        }

        return $this->renderAjax('result_soal', get_defined_vars());
    }

}
