<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user */

$activateLink = Yii::$app->urlManager->createAbsoluteUrl(['site/activate', 'token' => $user->password_reset_token]);
?>

Hello <?php echo Html::encode($user->username) ?>,

Follow the link below to activate your account:

<?php echo Html::a(Html::encode($activateLink), $activateLink) ?>