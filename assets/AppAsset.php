<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'js/datatables.net-bs/css/dataTables.bootstrap.css',
        'css/bootstrap-daterangepicker/daterangepicker.css',
        'css/amaya.css',
    ];
    public $js = [
        'js/datatables.net/js/jquery.dataTables.js',
        'js/datatables.net-bs/js/dataTables.bootstrap.js',
        'js/moment/min/moment.min.js',
        'css/bootstrap-daterangepicker/daterangepicker.js',
        'js/notify.js',
        'js/sweetalert.min.js',
        'js/amaya.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
