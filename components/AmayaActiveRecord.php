<?php 

namespace app\components;


use Yii;
use yii\db\ActiveRecord;

class AmayaActiveRecord extends ActiveRecord 
{

    public function beforeSave($insert=null)
    {
        if (parent::beforeSave($insert=null)) {
            if ($this->isNewRecord) {
                if ($this->hasAttribute('created_by')) {
                    $this->created_by = Yii::$app->user->identity->username;
                }
                if ($this->hasAttribute('created_dt')) {
                    $this->created_dt = date('Y-m-d H:i:s');
                }
            } else {
                if ($this->hasAttribute('updated_by')) {
                    $this->updated_by = Yii::$app->user->identity->username;
                }
                if ($this->hasAttribute('updated_dt')) {
                    $this->updated_dt = date('Y-m-d H:i:s');
                }
            }
            return true;
        }
        return false;
    }
}

?>