<?php 

namespace app\components;
use Yii;

class AmayaController extends \yii\web\Controller
{
    public $title;

    

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
             return false;
        }
        // your custom code here
        //Eg
        if(Yii::$app->user->isGuest){
             $this->redirect('/site/login');
             return false; //not run the action
        }

        return true; // continue to run action
    }
}

?>
