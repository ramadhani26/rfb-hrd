<?php 

namespace app\components;


use Yii;

class AmayaHelpers  
{
    public static function getUmur($birthDate, $compareDate=null)
    {
        $birthDate = date('Y-m-d', strtotime($birthDate));
        $compareDate = $compareDate ? date('Y-m-d', strtotime($compareDate)) : date('Y-m-d');
        $diff = date_diff(date_create($birthDate), date_create($compareDate));

        return $diff->y." tahun ".$diff->m." bulan ".$diff->d." hari";
    }
}