<?php

/* @var $this yii\web\View */

$this->title = 'About RFB Bandung';
?>
<div class="site-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
    </div>

    <div class="box-body" style="height: 90vh;">
        <p>
        PT. Rifan Financindo Berjangka (RFB) Bandung adalah salah satu kantor cabang dari PT. RFB yang terletak di ibukota Jawa Barat dan merupakan perusahaan kantor cabang pialang berjangka terbesar dan terdepan di Kota Bandung.
        <br/>
        <br/>Berdiri sejak tahun 2005, RFB Bandung merupakan anggota dari bursa berjangka yang ada di Indonesia yaitu Bursa Berjangka Jakarta (BBJ) serta anggota dari Kliring Berjangka Indonesia (KBI).
        <br/>
        <br/>Sebagai perusahaan pialang berjangka yang terpercaya, RFB Bandung berkomitmen untuk melaksanakan perdagangan berjangka secara teratur, wajar, efektif, dan transparan yang diatur dalam undang-undang di bidang perdagangan berjangka untuk memberikan kepastian hukum bagi semua pihak dalam kegiatan Perdagangan Berjangka di Indonesia khususnya di daerah Jawa Barat.
        <br/>
        <br/>Saat ini RFB Bandung didukung teknologi informasi yang mumpuni dan sumber daya manusia profesional yang memenuhi standar kualifikasi kepatutan dan kompetensi Badan Pengawas Perdagangan Berjangka Komoditi (BAPPEBTI). Dengan keunggulan sebagai perusahaan pialang berjangka teraktif dan berkinerja baik, RFB Bandung menjadi pilihan terbaik untuk investor yang ingin terlibat dalam industri perdagangan berjangka di Indonesia.
        </p>
    </div>
</div>
