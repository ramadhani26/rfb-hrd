$('#range-picker').daterangepicker({
    locale: {
        separator: ' s.d ',
        format: 'DD-MM-YYYY'
    }
});


$('.btn-cari').click(function() {
    // var params = $('#range-picker').val().split(' s.d ');
    var params = [
        $('#filter-from').val(),
        $('#filter-to').val(),
        $('#room_id').val(),
        $('#sbc_id').val(),
    ];
    console.log(params);
    window.location.replace('/dashboard/index?from='+params[0]+'&to='+params[1]+'&room='+params[2]+'&sbc='+params[3]);
});