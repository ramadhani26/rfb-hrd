<?php 
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\ArrayHelper;
use dosamigos\chartjs\ChartJs;
use kartik\date\DatePicker; 
$this->title = 'Dashboard';
 ?>

<div class="box-header with-border">
  <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
</div>

<div class="box-body">
    
    <?= Html::beginForm('', 'GET') ?>
        <!-- <div class="col-md-6">
            <div class="form-group">
                <label>Periode:</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" id="range-picker" value="<?= $filterForm; ?>">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat btn-cari">Cari</button>
                    </span>

                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="col-md-3 col-xs-6">
                <label>Periode:</label>
                <!-- <input type="text" class="form-control datepicker" id="filter-from"> -->
                <?php 
                echo DatePicker::widget([
                    'name' => 'filter-from', 
                    'value' => date('d-m-Y', strtotime('-1 months')),
                    'options' => ['placeholder' => 'Dari', 'id'=>'filter-from'],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        // 'todayHighlight' => true
                    ],
                    'removeButton' => false,
                    'readonly' => true,
                ]); ?>
            </div>
            <div class="col-md-3 col-xs-6">
                <label>&nbsp;</label>
                <?php 
                echo DatePicker::widget([
                    'name' => 'filter-from', 
                    'value' => date('d-m-Y'),
                    'options' => ['placeholder' => 'Sampai', 'id'=>'filter-to'],
                    'pluginOptions' => [
                        'format' => 'dd-mm-yyyy',
                        // 'todayHighlight' => true
                    ],
                    'removeButton' => false,
                    'readonly' => true,
                ]); ?>
            </div>
            <div class="col-md-3">
                <label>Room</label>
                <?= Html::dropDownList('filter-room',$getRoomId, $roomList, ['class'=>'form-control','id'=>'room_id', 'prompt'=>'-']); ?>
            </div>
            <div class="col-md-3">
                <label>SBC</label>
                <?= Html::dropDownList('filter-sbc',$getSbcId, $sbcList, ['class'=>'form-control','id'=>'sbc_id', 'prompt'=>'-']); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-info btn-cari pull-right">Cari</button>
            </div>
        </div>

    <?= Html::endForm() ?>

    <?= ChartJs::widget([
        'type' => 'bar',
        'options' => [
            'height' => 200,
            'width' => 400,
        ],
        'data' => [
            'labels' => $arrManager,
            'datasets' => [
                [
                    'label' => "Trainee",
                    'backgroundColor' => "rgba(179,181,198,1)",
                    'borderColor' => "rgba(179,181,198,1)",
                    'pointBackgroundColor' => "rgba(179,181,198,1)",
                    'pointBorderColor' => "#fff",
                    'pointHoverBackgroundColor' => "#fff",
                    'pointHoverBorderColor' => "rgba(179,181,198,1)",
                    // 'data' => [65, 59, 90]
                    'data' => $cntTrainee
                ],
                [
                    'label' => "MT / BC",
                    'backgroundColor' => "rgba(255,99,132,1)",
                    'borderColor' => "rgba(255,99,132,1)",
                    'pointBackgroundColor' => "rgba(255,99,132,1)",
                    'pointBorderColor' => "#fff",
                    'pointHoverBackgroundColor' => "#fff",
                    'pointHoverBorderColor' => "rgba(255,99,132,1)",
                    // 'data' => [28, 48, 40]
                    'data' => $cntStaff
                ]
            ]
        ]
    ]);
    ?>
</div>

<?php
$this->registerJs($this->render('js/index.js'), View::POS_END);
?>
