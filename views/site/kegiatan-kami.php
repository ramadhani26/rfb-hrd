<?php

/* @var $this yii\web\View */

$this->title = 'Kegiatan Kami';
?>
<div class="site-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
    </div>

    <div class="box-body" style="overflow-y:scroll">

        <div class="content-category">                                                                    
            <div class="container">
                <div class="row">

                    <section id="news" class="mb-4">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row mt-4">

                                        <div class="col-lg-3">
                                            <img class="img-fluid rounded" style='width:250px;height:150px' src="/uploads/127904.jpg" alt="Rayakan HUT RI ke-74, Seluruh Cabang RFB Serentak Gelar Aneka Lomba">
                                            <div class="recent-posts mt-4">
                                                <article class="post">
                                                    <div class="date">
                                                        <span class="day">08</span>
                                                        <span class="month bgred">Oct</span>
                                                    </div>
                                                    <h4>
                                                        <a href="javascript:void(0)">
                                                                Diundang Tim Marketing RFB AXA, Ifan Seventeen Kunjungi Kantor RFB
                                                        </a>
                                                    </h4>
                                                    <p>
                                                        Selasa, 1 Oktober 2019 – Rugi berinvestasi di perusahaan pialang berjangka tak membuatnya nya kapok, melainkan penasaran untuk terjun sebagai broker profesional. Kisah ini dialami dan dibagikan langsung oleh Riefian Fajarsyah atau yang akrab di sapa Ifan Seventeen saat berkunjung ke kantor PT. ...											
                                                    </p>                                                  
                                                </article>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <img class="img-fluid rounded" style='width:250px;height:150px' src="/uploads/127904.jpg" alt="Rayakan HUT RI ke-74, Seluruh Cabang RFB Serentak Gelar Aneka Lomba">
                                            <div class="recent-posts mt-4">
                                                <article class="post">
                                                    <div class="date">
                                                        <span class="day">08</span>
                                                        <span class="month bgred">Oct</span>
                                                    </div>
                                                    <h4>
                                                        <a href="javascript:void(0)">
                                                        RFB Ajak Media Pahami Peluang dan Risiko Investasi Perdagangan  Berjangka Komodi ...
                                                        </a>
                                                    </h4>
                                                    <p>
                                                    Bandung - PT Rifan Financindo Berjangka (“RFB”) cabang Bandung memberikan edukasi Perdagangan Berjangka Komoditi&nbsp; (PBK) dan Produk Derivatif Indeks kepada rekan-rekan media di Bandung pada Kamis,12 September 2019.
            Kegiatan ini merupakan rangkaian program edukasi RFB untuk lebih m ...												
                                                    </p>                                                  
                                                </article>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <img class="img-fluid rounded" style='width:250px;height:150px' src="/uploads/127904.jpg" alt="Rayakan HUT RI ke-74, Seluruh Cabang RFB Serentak Gelar Aneka Lomba">
                                            <div class="recent-posts mt-4">
                                                <article class="post">
                                                    <div class="date">
                                                        <span class="day">08</span>
                                                        <span class="month bgred">Oct</span>
                                                    </div>
                                                    <h4>
                                                        <a href="javascript:void(0)">
                                                        Tim Corpcomm RFB Adakan Kegiatan Internal Training dan Media Relation di Palemba ...
                                                        </a>
                                                    </h4>
                                                    <p>
                                                    Palembang – Setelah Yogyakarta, kini giliran RFB Palembang mendapat lawatan dari tim Corporate Communication. Selain untuk Media Visit ke kantor redaksi Palembang Ekspres, juga digelar acara Media Training yang dihadiri 26 jurnalis dari berbagai media, juga kegiatan pelatihan untuk tim Busin ...
                                                    </p>                                                  
                                                </article>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <img class="img-fluid rounded" style='width:250px;height:150px' src="/uploads/127904.jpg" alt="Rayakan HUT RI ke-74, Seluruh Cabang RFB Serentak Gelar Aneka Lomba">
                                            <div class="recent-posts mt-4">
                                                <article class="post">
                                                    <div class="date">
                                                        <span class="day">08</span>
                                                        <span class="month bgred">Oct</span>
                                                    </div>
                                                    <h4>
                                                        <a href="javascript:void(0)">
                                                        Rayakan HUT RI ke-74, Seluruh Cabang RFB Serentak Gelar Aneka Lomba
                                                        </a>
                                                    </h4>
                                                    <p>
                                                    Jakarta - Menyambut hari Kemerdekaan Republik Indonesia ke-74, pada 17 Agustus 2019 seluruh cabang PT Rifan Financindo Berjangka menggelar aneka lomba serentak. Di antaranya perlombaan makan kerupuk, membawa kelereng, memasukkan paku ke dalam botol, dan yel-yel antar tim marketing.
                                                    </p>                                                  
                                                </article>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    
                </div>

            </div>
        </div>

    </div>
</div>
