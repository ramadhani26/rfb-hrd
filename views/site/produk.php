<?php

/* @var $this yii\web\View */

$this->title = 'Produk';
?>
<div class="site-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
    </div>

    <div class="box-body" style="overflow-y:scroll">
        <h3>Produk Multilateral JFX</h3>
        <div class="row">
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3></h3>

                        <h4>KONTRAK<br>BERJANGKA<br>EMAS (GOL)</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4>KONTRAK<br>BERJANGKA <br>EMAS 250 GRAM <br>(GOL250)</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4>KONTRAK<br>BERJANGKA <br>OLEIN (OLE)</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
        </div>

        <hr>
        <h3>Produk Bilateral (SPA)</h3>
        <div class="row">
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3></h3>

                        <h4>AU1010_BBJ & AU10F_BBJ</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4>EU1010_BBJ & EU10F_BBJ</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4>GU1010_BBJ & GU10F_BBJ</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4>HKK50_BBJ & HKK5U_BBJ</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4>JPK50_BBJ & JPK5U_BBJ</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4>UC1010_BBJ & UC10F_BBJ</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4>UJ1010_BBJ & UJ10F_BBJ</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
            <div class="col-md-3">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4>XUL10 & XULF</h4>
                    </div>
                    <!-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
                </div>
            </div>
        </div>
    </div>
</div>
