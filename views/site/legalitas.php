<?php

/* @var $this yii\web\View */

$this->title = 'Legalitas';
?>
<div class="site-index">
    <div class="box-header with-border">
        <!-- <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3> -->
    </div>

    <div class="box-body" style="overflow-y:scroll">
    <b>Berikut adalah nama nama wakil Pialang Berjangka dari Tim Yudi Permana</b>
        <br/>1. Yudi Permana
        <br/>2. Mega Sukma Rahayu
        <br/>3. Muhammad Luthfi Jundiaturridwan
        <br/>4. Euis Faridatul Aliyah
        <br/>5. Andamsary Rahayu Setiawati
        <br/>6. Stephanie Marcelia Victoria

    <br/>
    <br/>
    
		<p><span style="color: #000000;"><strong><span style="font-family: 'Open Sans',sans-serif; font-size: 12pt;">Legalitas</span></strong></span></p>
        <p style="padding-left: 60px;"><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;">- Akta Perubahan Anggaran Dasar PT. Rifan Financindo Komoditas No. 32, pada 7 Maret 2000 oleh Notaris Linda Ibrahim SH.</span></p>
        <p style="padding-left: 60px;"><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;">- Pengesahan Departemen Hukum dan Perundang Undangan Republik Indonesia No : C-21254 HT.01.04.TH.2000</span></p>
        <p style="padding-left: 60px;"><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;">- Surat Persetujuan Anggota Bursa (SPAB) di Bursa Berjangka Jakarta No : SPAB-024 / BBJ / 09/00</span></p>
        <p style="padding-left: 60px;"><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;">- Izin Usaha Pialang Berjangka : Keputusan Kepala BAPPEBTI No : 08 / BAPPEBTI / SI / XII / 2000</span></p>
        <p style="padding-left: 60px;"><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;">- Anggota Lembaga Kliring Berjangka No : 03 / AK - KJBK / XII / 2000</span></p>
        <p style="padding-left: 60px;"><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;">- Peraturan Kepala BAPPEBTI Nomor 5 Tahun 2017 tentang Sistem Perdagangan Alternatif (SPA)</span></p>
        <p style="padding-left: 60px;"><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;">- Perjanjian Kerjasama dengan Pedagang Penyelenggara SPA, PT. Royal Assetindo No : 017 / KOM / RFB-RA / III / 2006</span></p>
        <p style="padding-left: 60px;"><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;">- Pemberian persetujuan sebagai peserta SPA dari BAPPEBTI No : 1162 / BAPPEBTI / SP / 5 / 2007</span></p>
        <p style="padding-left: 60px;"><span style="font-family: 'Open Sans', sans-serif; font-size: 10pt;"></span><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;">- Penetapan sebagai Pialang Berjangka yang melakukan kegiatan penerimaan calon nasabah secara Elektronik On-Line di bidang perdagangan Berjangka dan Komoditi kepada PT Rifan Financindo Berjangka No : 28 / BAPPEBTI / KEP-PBK / 09 / 2014</span></p>
        <p style="padding-left: 60px;"><span style="color: #000000;"></span></p>
        <p><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;">Catatan:</span></p>
        <p style="padding-left: 60px;"><span style="font-family: 'Open Sans',sans-serif; font-size: 10pt; color: #000000;"><strong>BAPPEBTI </strong>( Badan Pengawas Perdagangan Berjangka Komoditi )</span></p>	

    </div>

    
    <!-- <div class="nav-tabs-custom">
        <ul class="nav nav-tabs nav-justified flex-wrap">
              <li class="active"><a href="#tab_1" data-toggle="tab">Tentang Kami</a></li>
              <li><a href="#tab_2" data-toggle="tab">Sistem Penerimaan Karyawan</a></li>
              <li><a href="#tab_3" data-toggle="tab">Produk</a></li>
              <li><a href="#tab_4" data-toggle="tab">Legalitas</a></li>
              <li><a href="#tab_5" data-toggle="tab">Info dan Kegiatan</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1" style="height: 90vh">
                <iframe src="https://www.youtube.com/embed/RWDEw0kIaEI" style="width: 100%; height: 100%;">
                </iframe>
            </div>
            <div class="tab-pane" id="tab_2">test tab 2</div>
            <div class="tab-pane" id="tab_3">test tab 3</div>
            <div class="tab-pane" id="tab_4">test tab 4</div>
            <div class="tab-pane" id="tab_5">test tab 5</div>
        </div>
    </div> -->
</div>
