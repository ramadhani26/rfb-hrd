<?php 
use yii\helpers\Html; 
$this->title = 'Login RFB-BDG';
?>

<style>
    @import url('https://fonts.googleapis.com/css?family=Josefin+Sans&display=swap');
    body, html  {
        height: 100% !important;
        margin: 0;
        font-family: 'Josefin Sans';
    }
    .bg {
        /*height: 100%;
        background-color: #173f5f;
        color: white;*/
        width: 100%;
        height: 100vh;
        background-position: center center;
        background-size: cover;
        background-repeat: no-repeat;
        backface-visibility: hidden;
        animation: slideBg 60s linear infinite 0s;
        animation-timing-function: ease-in-out;
        background-image: url('/img/bg/1.jpg');
        color: #666666 !important;
    }
    @keyframes slideBg {
        0% {
            background-image: url('/img/bg/1.jpg');
        }
        20% {
            background-image: url('/img/bg/2.jpg');
        }
        40% {
            background-image: url('/img/bg/3.jpg');
        }
        60% {
            background-image: url('/img/bg/4.jpg');
        }
        80% {
            background-image: url('/img/bg/5.jpg');
        }
        100% {
            background-image: url('/img/bg/6.jpg');
        }
    }

    .caption {
        position: absolute;
        left: 0;
        top: 30%;
        width: 100%;
        text-align: center;
        font-size: 2em;
    }
</style>
<div class='bg'>
    <div class="caption">
        <!-- <H2>PT. RIFAN FINANCINDO BERJANGKA</H2> -->
        <!-- <H3>KANTOR CABANG BANDUNG</H3> -->
        PT. RIFAN FINANCINDO BERJANGKA
         <br>
        <small>Bandung</small>
         <br>
         <br>
         <?= Html::a('Login', '/site/login', ['class'=>'btn btn-success btn-lg']); ?>
         <?= Html::a('Sign Up', '/site/signup', ['class'=>'btn btn-info btn-lg']); ?>
    </div>
</div>