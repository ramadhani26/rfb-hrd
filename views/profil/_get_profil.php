<?php 

use yii\web\View; 
use yii\helpers\Html; 
?>
<style type="text/css">
    .dl-horizontal dt {
        text-align: left;
    }
</style>

<h3 style="margin-top: -10px;"><?= $model->nama_lengkap; ?><small> - <?= $model->user->userRole->name; ?> </small></h3>

<div class="box box-warning">
    <div class="box-header">
        
    </div>
    <div class="box-body">
        <div class="col-md-6">
            <table class="table table-responsive">
                <tbody>
                    <tr>
                        <th width="40%">Jenis Identitas</th>
                        <td><?= $model->jenis_identitas ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Nomor Identitas</th>
                        <td><?= $model->nomor_identitas ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td><?= $model->alamat_identitas ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Alamat Sekarang</th>
                        <td><?= $model->alamat_sekarang ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Tempat, Tgl Lahir</th>
                        <td><?= ($model->tempat_lahir ? : '-') . ($model->tanggal_lahir ? ', ' . date('d M Y', strtotime($model->tanggal_lahir)) : ''); ?></td>
                    </tr>
                    <tr>
                        <th>Jenis Kelamin</th>
                        <td><?= $model->jenis_kelamin ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>No. Telepon</th>
                        <td><?= $model->no_telepon ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>No. Handphone</th>
                        <td><?= $model->no_handphone ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><?= $model->email ? : '-'; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table table-responsive">
                <tbody>
                    <tr>
                        <th width="40%">Status Perkawinan</th>
                        <td><?= $model->status_perkawinan ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Ayah Kandung</th>
                        <td><?= $model->ayah_kandung ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Ibu Kandung</th>
                        <td><?= $model->ibu_kandung ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Alamat Keluarga</th>
                        <td><?= $model->keluarga_alamat ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th>Nama Keluarga yang Bisa Dihubungi</th>
                        <td><?= $model->keluarga_nama ? : '-'; ?></td>
                    </tr>                    
                    <tr>
                        <th>No. Telepon Keluarga</th>
                        <td><?= $model->keluarga_no_telepon ? : '-'; ?></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Posisi yang dilamar</th>
                        <td><?= $model->posisi ? : '-'; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="box-footer">
        <?php if ($model->nama_file && $model->user->user_role_id == 4) : ?>
            <!-- <a type="button" href='profil/' class="btn btn-success btn-download-documents" data-id="<?= $model->id; ?>">Download Dokumen</button> -->
            <?php 
            echo Html::a('Download Dokumen', 'profil/download-dokumen?id=' . $model->id, ['class'=>'btn btn-success']);
            ?>
        <?php endif; ?>
    </div>
</div>

<?php 
$this->registerJs($this->render('js/get_profil.js'), View::POS_END);
?>