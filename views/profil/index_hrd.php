<?php
use yii\web\View; 
use yii\helpers\Html; 
$this->title = 'Profil HRD';
?>

<div class="box-header with-border">
    <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
</div>

<div class="box-body">
    <div class="row">
        <div class="col-md-4">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active" style='background-color: #086311 !important'>
                    <h3 class="widget-user-username"><?= $profil->nama_lengkap; ?></h3>
                    <h5 class="widget-user-desc"><?= $profil->user->userRole->name; ?></h5>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" src="/img/profile/default.jpg" alt="User Avatar">
                </div>
                <br>
                <div class="box-footer">
                    <!-- <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header"><?= count($followerActives) + count($followerTrainees); ?></h5>
                                <span class="description-text">Total Tim</span>
                            </div>
                        </div>
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header"><?= count($followerTrainees); ?></h5>
                                <span class="description-text">Trainee</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header"><?= count($followerActives); ?></h5>
                                <span class="description-text">Karyawan</span>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    </div>

    <!-- managers -->
    <?php if ($managers) : ?>
        <h3 class="box-title">SBC</h3>
        <div class="row">
            <?php foreach ($managers as $manager) : ?>
            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-default">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="/img/profile/default.jpg" alt="User profile picture">
                        <h3 class="profile-username text-center"><?= $manager['nama_lengkap']; ?></h3>
                        <p class="text-muted text-center"><?= $manager['user']['userRole']['name']; ?></p>
                    </div>
                    <div class="box-footer">
                        <?php $total = $followerTrainees[$manager['id']] + $followerActives[$manager['id']]; ?>
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">
                                        <?= $followerTrainees[$manager['id']] + $followerActives[$manager['id']] ?></h5>
                                    <span class="description-text">Total Tim</span>
                                </div>
                            </div>
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header"><?= $followerTrainees[$manager['id']]; ?></h5>
                                    <span class="description-text">Trainee</span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <h5 class="description-header"><?= $followerActives[$manager['id']]; ?></h5>
                                    <span class="description-text">MT / BC</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?= html::a('Info SBC', '/info-manager/detail-manager?id='.$manager['id'], ['class'=>'btn btn-info btn-sm']); ?>

                            <?= html::button('Hapus SBC', ['class'=>'btn btn-danger btn-sm pull-right btn-delete-account ' . ($total ? 'disabled' : ''), 'data-id'=>$manager->id]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>


<div class="modal fade" id="modal-profile">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="modal-profile-content">
                
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php
$this->registerJs($this->render('js/index.js'), View::POS_END);
?>
