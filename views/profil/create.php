<?php

use yii\web\View;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\BreadCrumbs;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Profil */
/* @var $form ActiveForm */
$this->title = 'Tambah Anggota';
?>
<div class="box-header with-border">
  <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
</div>

<!-- form start -->
<div class="profil-create">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                // 'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-md-7 col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>
    <div class="box-body">
            <?= $form->field($model, 'nama_lengkap') ?>
            <?= $form->field($model, 'alamat_sekarang')->textArea() ?>
            <?= $form->field($model, 'jenis_kelamin')->inline()->radioList($listJenisKelamin) ?>
            <?= $form->field($model, 'tempat_lahir')->widget(Select2::classname(), [
                'theme' => Select2::THEME_BOOTSTRAP,
                'pluginOptions' => [
                    'allowClear' => false,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Loading...'; }"),
                    ],
                    'ajax' => [
                        'url' => \yii\helpers\Url::to(['/profil/get-kota']),
                        'dataType' => 'json',
                        'delay' => 500,
                        'data' => new JsExpression('
                            function(params) {
                                return {
                                    q:params.term
                                };
                            }
                        ')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    // 'templateResult' => new JsExpression(
                    //     'function(no_rekam_medik) {
                    //         return no_rekam_medik.text;
                    //     }'
                    // ),
                    // 'templateSelection' => new JsExpression(
                    //     'function (no_rekam_medik) {
                    //         $(".pendaftaran-id").val(no_rekam_medik.pendaftaran_id);
                    //         return no_rekam_medik.text;
                    //     }'
                    // ),
                ],
            ]); ?>
            <?= $form->field($model, 'tanggal_lahir')->widget(DatePicker::class, [
                //'language' => 'ru',
                'dateFormat' => 'dd-MM-yyyy',
                'options' => [
                    'class'=>'form-control',
                ],
                'clientOptions' => [
                    'changeMonth' => true,

                    'yearRange' => '1970:2099',
                    'changeYear' => true,
                ]
            ]) ?>
            <?= $form->field($model, 'umur', ['inputOptions'=>['readonly'=>true]]) ?>

            <?= $form->field($model, 'no_handphone', ['inputOptions'=>['onkeyup'=>"numOnly(this)", 'onblur'=>"numOnly(this)"]]) ?>
            <?php if ($userRoleId == 5) : ?>
                <?= $form->field($model, 'posisi')->dropDownList(
                    $listPosisi,
                    [
                        'prompt'=>'--Pilih Posisi--'
                    ]) ?>
                <?= $form->field($model, 'posisi_lainnya')->label('') ?>
                <?= $form->field($model, 'nama_file_cv')->fileInput(['accept' => '.jpg, .png, .pdf']) ?>
                <?= $form->field($model, 'nama_file_ktp')->fileInput(['accept' => '.jpg, .png, .pdf']) ?>
                <?= $form->field($model, 'nama_file_kk')->fileInput(['accept' => '.jpg, .png, .pdf']) ?>
                <?= $form->field($model, 'nama_file_ijazah')->fileInput(['accept' => '.jpg, .png, .pdf']) ?>
                <!-- <?= $form->field($model, 'nama_file_skck')->fileInput(['accept' => '.jpg, .png, .pdf']) ?> -->
                
            <?php elseif ($userRoleId == 3): ?>
                <?= $form->field($model, 'manager_room_id')->dropDownList(
                    $listRoom,
                    [
                        'prompt'=>'--Pilih Room--'
                    ]) ?>
            <?php endif; ?>
    </div><!-- profil-create -->
    <div class="box-footer">
        <div class="col-md-7 col-sm-offset-4 col-sm-8">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary pull-right']) ?>
        <!-- </div> -->
    </div>
    <?php ActiveForm::end(); ?>
</div>


<?php 
$this->registerJs($this->render('js/create.js'), View::POS_END);
?>

