<?php

use yii\helpers\Html;
use yii\web\View; 
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Profil */
/* @var $form ActiveForm */
?>

<!-- form start -->
<div class="profil-set-manager">
    <?php $form = ActiveForm::begin([
        'id' => 'set-manager-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                // 'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-md-7 col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>
    <div class="box-body">
        <?= $form->field($model, 'manager_id')->dropDownList(
            $managerList,
            [
                'prompt'=>'--Pilih SBC--'
            ]
        ) ?>
        <?= $form->field($model, 'id')->hiddenInput(['value'=>$id])->label(false) ?>
    </div><!-- profil-create -->
    <div class="box-footer">
        <div class="col-md-7 col-sm-offset-4 col-sm-8">
            <?= Html::button('Simpan', ['class' => 'btn btn-primary btn-set-manager pull-right']) ?>
        <!-- </div> -->
    </div>
    <?php ActiveForm::end(); ?>
</div>


<?php 
$this->registerJs($this->render('js/set_manager.js'), View::POS_END);
?>

