<?php
use yii\web\View; 
use yii\helpers\Html; 
$this->title = 'Profil Trainee';
?>


<style type="text/css">
    .myBtnFloat {
      display: block;
      position: fixed;
      bottom: 45px;
      right: 30px;
      z-index: 99;
    }
    .btn-circle.btn-xl {
        width: 70px;
        height: 70px;
        padding: 10px 16px;
        border-radius: 35px;
        font-size: 24px;
        line-height: 1.33;
    }
</style>

<div class="box-header with-border">
    <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
</div>

<div class="box-body">
    <div class="row">
        <div class="col-md-4">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active" style='background-color: #086311 !important'>
                    <h3 class="widget-user-username"><?= $profil->nama_lengkap; ?></h3>
                    <h5 class="widget-user-desc"><?= $profil->user->userRole->name; ?>
                        <?php if ($profil && $profil->user->user_role_id == 5) : ?>
                            <span class="pull-right">
                                <?php 
                                $step1 = 'Step 1 : Set SBC';
                                $step2 = 'Step 2 : Pengisian Soal';
                                $step3 = 'Step 3 : Pengisian Form Fingerprint';
                                $step4 = 'Step 4 : Drop SBC';
                                if ($profil->manager_approved == 1)  {
                                    echo '<i class="fa fa-star" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step1.'<br>status : Berhasil"></i> ';
                                } elseif (!$profil->manager_id) {
                                    echo '<i class="fa fa-star-o" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step1.'<br>status : Belum dilakukan"></i> ';
                                } else {
                                    echo '<i class="fa fa-star-o" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step1.'<br>status : Belum diapprove"></i> ';
                                }

                                if ($profil->has_soal_submitted == 1) {
                                    if ($profil->has_soal_approved == 1) {
                                        echo '<i class="fa fa-star" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step2.'<br>status : Berhasil"></i> ';
                                    } elseif ($profil->has_soal_approved == 2) { // gagal di sesi soal
                                        if ($profil->response_recall != 1) {
                                            echo '<i class="fa fa-star-half-o" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step2.'<br>status : Gagal"></i> ';
                                        } else {
                                            echo '<i class="fa fa-star" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step2.'<br>status : Berhasil"></i> ';
                                        }
                                    } else {
                                        echo '<i class="fa fa-star-o" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step2.'<br>status : Proses Pengecekan HRD"></i>';
                                    }
                                } else {
                                    echo '<i class="fa fa-star-o" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step2.'<br>status : Belum Dilakukan"></i> ';
                                }

                                if ($profil->tanggal_finger) {
                                    echo '<i class="fa fa-star" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step3.'<br>status : Berhasil"></i> ';
                                } else {
                                    echo '<i class="fa fa-star-o" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step3.'<br>status : Belum Dilakukan"></i> ';
                                }

                                if ($profil->drop_manager) {
                                    echo '<i class="fa fa-star" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step4.'<br>status : Berhasil"></i> ';
                                } else {
                                    echo '<i class="fa fa-star-o" data-container="body" data-toggle="popover" data-trigger="hover" data-html=1 data-placement="bottom" data-content="'.$step4.'<br>status : Belum Dilakukan"></i> ';
                                }
                                ?>
                            </span>
                        <?php endif; ?>
                    </h5>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" src="/img/profile/default.jpg" alt="User Avatar">
                </div>
                <br>
                <div class="box-footer">
                    <?php 
                    if ($profil->l_status_profil_id == 1) { // belu diapprove HRD
                        echo '<span class="pull-right badge bg-orange">Menunggu Pengecekan Profil oleh HRD</span>';
                    } else {
                        if ($profil->manager_id == null) {
                            echo Html::a('Set SBC', null, [
                                'class'=>'btn btn-danger btn-sm btn-modal-set-manager pull-right',
                                'data-id'=>$profil->id,
                            ]);
                        } else {
                            if ($profil->manager_approved === 0) {// belum diapprove manager oleh HRD
                                echo '<div class="badge bg-orange">Menunggu Approve SBC</div>';
                                echo Html::a('Reset SBC', null, [
                                    'class'=>'btn btn-default btn-sm btn-modal-reset-manager pull-right',
                                    'data-id'=>$profil->id,
                                ]);
                            } else {
                                $room = $profil->manager->managerRoom ? $profil->manager->managerRoom->nama_room : '';
                                echo "<b>SBC</b> <p class=\"pull-right\">{$profil->manager->nama_lengkap} <br>Room $room</p>";
                            }
                        }
                    }
                    ?>
                </div>
            </div>
            <!-- /.widget-user -->
        </div>

        <?php if ($profil->user->user_role_id == 5 && $profil->manager_approved) : ?>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="info-box bg-green">
                    <span class="info-box-icon"><i class="fa fa-star"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Progress</span>
                        <span class="info-box-number">Training</span>

                        <div class="progress">
                            <?php
                            $progress = 25; 
                            if ($profil->has_soal_submitted) {
                                $progress += 25;
                            }
                            if ($profil->tanggal_finger) {
                                $progress += 25;
                            }
                            if ($profil->drop_manager) {
                                $progress += 25;
                            }
                            ?>
                            <div class="progress-bar" style="width: <?= $progress; ?>%"></div>
                        </div>
                        <span class="progress-description">
                            <?= $progress; ?>% Progress telah dijalani
                        </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->

            <?php 
            if ($profil->user->user_role_id == 5) {
                if ($profil->has_soal_showed == 0) {
                    $body = 'Tunggu tahap selanjutnya : <i>Isi Soal</>. <br>';
                    echo \yii\bootstrap\Alert::widget([
                        'body' => $body,
                        'closeButton' => false,
                        'options' => [
                            'id' => '',
                            'class' => 'alert-warning',
                        ],
                    ]);
                } else {
                    if ($profil->has_soal_submitted == 0) {
                        echo \yii\bootstrap\Alert::widget([
                            'body' => 'Klik Tombol aksi di pojok kanan bawah untuk mengisi soal yang sudah disediakan.',
                            'closeButton' => false,
                            'options' => [
                                'id' => '',
                                'class' => 'alert-info',
                            ],
                        ]);
                        echo Html::a('<i class="fa fa-pencil-square-o fa-lg"></i>', '/sesi-soal/isi', ['class'=>'btn btn-info btn-lg pull-right btn-circle btn-xl myBtnFloat']); 
                    } else {
                        if ($profil->has_soal_approved == 1 || ($profil->has_soal_approved == 2 && $profil->response_recall == 1)) {
                            if (!$profil->tanggal_finger) {
                                echo \yii\bootstrap\Alert::widget([
                                    'body' => 'Selamat! Anda Lulus tahap seleksi Pengisian Soal. Silakan datang kembali untuk mengikuti tahap selanjutnya : <i>Pengisian Data Fingerprint</i>.',
                                    'closeButton' => false,
                                    'options' => [
                                        'id' => '',
                                        'class' => 'alert-info',
                                    ],
                                ]);
                                echo Html::a('<i class="fa fa-id-badge fa-lg"></i>', '/finger-form/create?id='.$profil->id, ['class'=>'btn btn-circle btn-info btn-xl pull-right myBtnFloat']); 
                            } else {
                                if (!$profil->drop_manager) {
                                    echo \yii\bootstrap\Alert::widget([
                                        'body' => 'Tahap pengisian data fingerprint telah dilakukan. Silakan ikuti tahap selanjutnya : <i>Drop Manager</i>.<br><br>
                                        Data Form Fingerprint bisa di edit jika anda belum yakin atau ada perubahan. Tekan tombol action di pojok kanan bawah',
                                        'closeButton' => false,
                                        'options' => [
                                            'id' => '',
                                            'class' => 'alert-info',
                                        ],
                                    ]);
                                    echo Html::a('<i class="fa fa-id-badge fa-lg"></i>', '/finger-form/create?id='.$profil->id, ['class'=>'btn btn-circle btn-info btn-xl pull-right myBtnFloat']); 
                                } else {
                                    if (!$profil->is_lolos) {
                                        echo \yii\bootstrap\Alert::widget([
                                            'body' => 'Selamat! Anda telah berhasil mengikuti seluruh rangkaian kegiatan Rekrutmen & Training. Informasi selanjutnya akan dilakukan langsung oleh SBC atau HRD.',
                                            'closeButton' => false,
                                            'options' => [
                                                'id' => '',
                                                'class' => 'alert-success',
                                            ],
                                        ]);
                                    }
                                }
                            }

                        } elseif ($profil->has_soal_approved == 0) {
                            // echo '<span class="pull-right badge bg-orange">Jawaban Soal Sedang Diperiksa</span>';
                            echo \yii\bootstrap\Alert::widget([
                                'body' => 'Jawaban soal dalam tahap pemeriksaan. Harap menunggu informasi selanjutnya',
                                'closeButton' => false,
                                'options' => [
                                    'id' => '',
                                    'class' => 'alert-warning',
                                ],
                            ]);
                        } elseif ($profil->has_soal_approved == 2 && $profil->response_recall == 0) {
                            echo \yii\bootstrap\Alert::widget([
                                'body' => 'Jawaban soal dalam tahap pemeriksaan. Harap menunggu informasi selanjutnya',
                                // 'body' => 'Maaf anda gagal dalam tahap seleksi Pengisian Soal.',
                                'closeButton' => false,
                                'options' => [
                                    'id' => '',
                                    // 'class' => 'alert-danger',
                                    'class' => 'alert-warning',
                                ],
                            ]);
                        }
                    }
                }
            }
            ?>
            </div>

        <?php endif; ?>
    </div>
</div>


<div class="modal fade" id="modal-profile">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="modal-profile-content">
                
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php
$this->registerJs($this->render('js/index.js'), View::POS_END);
?>
