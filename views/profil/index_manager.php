<?php
use yii\web\View; 
use yii\helpers\Html; 
$this->title = 'Profil Manager';
?>

<style type="text/css">
    .box.box-default {
        height: 280px;
    }
</style>

<div class="box-header with-border">
    <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
</div>

<div class="box-body">
    <div class="row">
        <div class="col-md-4">
            <!-- Widget: user widget style 1 -->
            <div class="box box-widget widget-user">
                <!-- Add the bg color to the header using any of the bg-* classes -->
                <div class="widget-user-header bg-aqua-active" style='background-color: #086311 !important'>
                    <h3 class="widget-user-username"><?= $profil->nama_lengkap; ?></h3>
                    <h5 class="widget-user-desc"><?= $profil->user->userRole->name; ?></h5><?= $profil->managerRoom ? $profil->managerRoom->nama_room : '-';?>
                </div>
                <div class="widget-user-image">
                    <img class="img-circle" src="/img/profile/default.jpg" alt="User Avatar">
                </div>
                <br>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header"><?= ($followerActives ? count($followerActives) : 0) + ($followerTrainees ? count($followerTrainees) : 0); ?></h5>
                                <span class="description-text">Total Tim</span>
                            </div>
                        </div>
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header"><?= $followerTrainees ? count($followerTrainees) : 0; ?></h5>
                                <span class="description-text">Trainee</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header"><?= $followerActives ? count($followerActives) : 0; ?></h5>
                                <span class="description-text">MT / BC</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.widget-user -->
        </div>
    </div>

    <!-- follower trainee -->
    <h3 class="box-title">Anggota Tim</h3>
    <?php if ($followerTrainees) : ?>
        <div class="row">
            <?php foreach ($followerTrainees as $followerTrainee) : ?>
            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-default">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="/img/profile/default.jpg" alt="User profile picture">
                        <h3 class="profile-username text-center"><?= $followerTrainee->nama_lengkap; ?></h3>
                        <p class="text-muted text-center"><?= $followerTrainee->user->userRole->name; ?></p>

                        <?php if ($followerTrainee->user->user_role_id == 3) : ?> <!-- manager -->
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Total Anggota Tim</b> <a class="pull-right">1,322</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Jumlah Trainer</b> <a class="pull-right">543</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Jumlah Tim Masuk</b> <a class="pull-right">13,287</a>
                                </li>
                            </ul>
                        <?php elseif ($followerTrainee->user->user_role_id == 5) : ?>
                            <div class="text-center">
                                <?php 
                                if ($followerTrainee) {
                                    if ($followerTrainee->manager_approved == 1)  {
                                        echo '<i class="fa fa-star"></i> ';
                                    } else {
                                        echo '<i class="fa fa-star-o"></i> ';
                                    }

                                    if ($followerTrainee->has_soal_submitted == 1) {
                                        if ($followerTrainee->has_soal_approved == 1) { // berhasil lolos soal
                                            echo '<i class="fa fa-star"></i> ';
                                        } else if ($followerTrainee->has_soal_approved == 2) { // gagal lolos Soal
                                            if ($followerTrainee->response_recall != 1) {
                                                echo '<i class="fa fa-star-half-o"></i> ';
                                            } else {
                                                echo '<i class="fa fa-star"></i> ';
                                            }
                                        } else {
                                            echo '<i class="fa fa-star-o"></i> ';
                                        }
                                    } else {
                                        echo '<i class="fa fa-star-o"></i> ';
                                    }

                                } else {
                                    echo '<i class="fa fa-star-o"></i> ';
                                    echo '<i class="fa fa-star-o"></i> ';
                                } 
                                ?>
                                <i class="fa fa-star-o"></i>
                                <i class="fa fa-star-o"></i>
                            </div>
                        <?php endif; ?>

                        <button class="btn btn-primary btn-block btn-profile-detail" data-id="<?= $followerTrainee->id ?>">
                            <b>Lihat Detail</b>
                        </button>

                        <div id="request_to_recall-<?= $followerTrainee->id ?>">
                            <?php if ($followerTrainee->has_soal_approved == 2) : ?>
                                <?php if ($followerTrainee->request_to_recall == 0) : ?>
                                <button class="btn btn-warning btn-sm pull-right btn-request_recall" data-id="<?= $followerTrainee->id ?>" style="margin-top: 5px">Recall</button>
                                <?php else : ?>
                                    <?php if ($followerTrainee->response_recall == 0) : ?>
                                        <span class="pull-right badge bg-orange"  style="margin-top: 5px">On Progress Recall</span>
                                    <?php elseif ($followerTrainee->response_recall == 1) : ?>
                                        <span class="pull-right badge bg-green"  style="margin-top: 5px">Recall Success</span>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>



    <!-- MT / BC -->
    <h3 class="box-title">MT / BC</h3>
    <?php if ($followerActives) : ?>
        <div class="row">
            <?php foreach ($followerActives as $followerActive) : ?>
            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-default">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="/img/profile/default.jpg" alt="User profile picture">
                        <h3 class="profile-username text-center"><?= $followerActive->nama_lengkap; ?></h3>
                        <p class="text-muted text-center"><?= $followerActive->user->userRole->name; ?></p>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>


<div class="modal fade" id="modal-profile">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="modal-profile-content">
                
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php
$this->registerJs($this->render('js/index.js'), View::POS_END);
?>
