$('.btn-profile-detail').click(function() {
    var profil_id = $(this).data('id');
    $('.modal-title').html('');
    $.ajax({
        type: 'GET',
        url: window.location.origin + '/profil/get-profil?id=' + profil_id,
        // dataType: 'JSON',
        beforeSend: function () {
            $('#modal-profile').modal();
            $('#modal-profile-content').html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div></br>');
        },
        success: function (res) {
            $('#modal-profile-content').html(res);
        },
        complete: function () {
        }
    });
});

$('.btn-modal-set-manager').click(function() {
    var profil_id = $(this).data('id');
    $('.modal-title').html('Pilih SBC');
    $.ajax({
        type: 'GET',
        url: window.location.origin + '/profil/modal-set-manager?id=' + profil_id,
        // dataType: 'JSON',
        beforeSend: function () {
            $('#modal-profile').modal();
            $('#modal-profile-content').html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div></br>');
        },
        success: function (res) {
            $('#modal-profile-content').html(res);
        },
        complete: function () {
        }
    });
});

$('.btn-modal-reset-manager').click(function() {
    var profil_id = $(this).data('id');
    $('.modal-title').html('Pilih SBC');
    $.ajax({
        type: 'GET',
        url: window.location.origin + '/profil/modal-set-manager?id=' + profil_id,
        // dataType: 'JSON',
        beforeSend: function () {
            $('#modal-profile').modal();
            $('#modal-profile-content').html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div></br>');
        },
        success: function (res) {
            $('#modal-profile-content').html(res);
        },
        complete: function () {
        }
    });
});

$('.btn-request_recall').click(function() {
    var _htmlBtn = $(this).html();
    // console.log(_htmlBtn); return false;
    var profil_id = $(this).data('id');
    $.ajax({
        type: 'GET',
        url: window.location.origin + '/profil/request-recall?id=' + profil_id,
        // dataType: 'JSON',
        beforeSend: function () {
            $('.btn-request_recall').html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
        },
        success: function (res) {
            setTimeout( function() {
                $('.btn-request_recall').html(_htmlBtn).attr('disabled', false);
                $.notify("Request untuk recall berhasil.", "success");
                $('.btn-request_recall').addClass("hidden");
                $('#request_to_recall-'+profil_id).html('<span class="pull-right badge bg-orange"  style="margin-top: 5px">On Progress Recall</span>');

            }, 1000);
        },
        // complete: function () {
        // }
    });
});

$('.btn-delete-account').click(function() {
    if ($(this).hasClass('disabled')) return;

    var _htmlBtn = $(this).html();
        var profil_id = $(this).data('id');
        swal("Apakah anda yakin untuk menghapus profil ini?", {
            buttons: {
                cancel: "Tidak", 
                yes: "Ya",
            },
            icon: "warning",
            dangerMode: true,
        }).then((value) => {
            switch (value) {
                case ('yes'):
                    $.ajax({
                        type: 'GET',
                        url: window.location.origin + '/info-trainee/delete-profil?id=' + profil_id,
                        // dataType: 'JSON',
                        beforeSend: function () {
                            $('.btn-delete-account').html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
                        },
                        success: function (res) {
                            $.notify("Hapus Profil berhasil.", "success");
                            $('.btn-delete-account').html(_htmlBtn).attr('disabled', false);
                            // window.location.reload();
                            setTimeout(function() {
                                window.location.reload();
                            }, 1500);
                        },
                        complete: function () {
                        }
                    });
                    break;
            }
        });
});


