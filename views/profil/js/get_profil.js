
$('.btn-download-documents').click(function() {
    var id = $(this).data('id');
    var tempHtml = '';
    $.ajax({
        type: 'GET',
        url: window.location.origin + '/profil/download-dokumen?id=' + id,
        beforeSend: function () {
            tempHtml = $(this).html();
            $(this).html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
        },
        success: function (res) {
            window.location =  window.location.origin + '/profil/download-dokumen?id=' + id;
        },
        complete: function () {
            $(this).html(tempHtml).attr('disabled', false);
        }
    });
});