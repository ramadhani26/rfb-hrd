
$('.btn-set-manager').click(function() {
    var data = $('#set-manager-form').serializeArray();
    $.ajax({
        type: 'POST',
        url: window.location.origin + '/profil/set-manager',
        data: data,
        dataType: 'JSON',
        beforeSend: function () {
            $('.btn-set-manager').html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
        },
        success: function (res) {
        },
        complete: function () {
            $('.btn-set-manager').html('Simpan').attr('disabled', false);
            window.location.reload();
        }
    });
});


$('.modal-title').html("Pilih SBC");