
$(document).ready(function() {
    if ($('#profilawalform-posisi').val() == 'Lainnya') {
        $('.field-profilawalform-posisi_lainnya').show();
    } else {
        $('.field-profilawalform-posisi_lainnya').hide();
    }
});

$('#profilawalform-posisi').change(function() {
    if ($(this).val() == 'Lainnya') {
        $('.field-profilawalform-posisi_lainnya').show();
    } else {
        $('.field-profilawalform-posisi_lainnya').hide();
    }
});

$('#profilawalform-tanggal_lahir').change(function() {
    if ($(this).val() != '') {
        $.ajax({
            type: 'GET',
            url: window.location.origin + '/profil/get-umur?tgl_lahir=' + $(this).val(),
            // dataType: 'JSON',
            beforeSend: function () {
                $('#profilawalform-umur').val('loading...');
            },
            success: function (res) {
                $('#profilawalform-umur').val(res);
            },
            complete: function () {
            }
        });
    } else {
        $('#profilawalform-umur').val('');
    }
});

function numOnly(selector){
  selector.value = selector.value.replace(/[^0-9]/g,'');
}
