var table;
$(document).ready(function() {
    table = $('#table').DataTable( {
        "ajax": "/info-manager/get-data",
        "columnDefs": [
          { className: "dt-nowrap text-nowrap", "targets": [ 0, 2, 3, 4, 5] }
        ],
        'paging' : true,
        'scrollX' : true,
    "bAutoWidth": false,
        // 'scrollY' : true,
        "pagingType": "simple_numbers",
        "columns": [
            { "data": "nama_lengkap" },
            { "data": "alamat_sekarang" },
            { "data": "no_telepon" },
            { "data": "no_handphone" },
            { "data": "ruangan", 'sortable':false },
            { "data": "aksi", 'sortable':false  },
            // { "data": "akun", 'sortable':false  },
        ]
    } );
});