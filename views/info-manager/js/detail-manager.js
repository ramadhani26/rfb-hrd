var table;
$(document).ready(function() {
    table = $('#table').DataTable( {
        "ajax": "/info-manager/get-data-anggota?id=" + manager_id,
        "columnDefs": [
          { className: "dt-nowrap text-nowrap", "targets": [ 0, 1, 2 ] }
        ],
        'paging' : true,
        'scrollX' : true,
        // 'scrollY' : true,
        "pagingType": "simple_numbers",
        "columns": [
            { "data": "nama_lengkap" },
            { "data": "status", 'sortable':false  },
            { "data": "aksi", 'sortable':false  },
        ]
    } );

    $('#table').on('click', '.btn-modal-reset-manager', function() {
        var profil_id = $(this).data('id');
        $('.modal-title').html('Pilih SBC');
        $.ajax({
            type: 'GET',
            url: window.location.origin + '/info-manager/modal-set-manager?id=' + profil_id,
            // dataType: 'JSON',
            beforeSend: function () {
                $('#modal-profile').modal();
                $('#modal-profile-content').html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div></br>');
            },
            success: function (res) {
                $('#modal-profile-content').html(res);
            },
            complete: function () {
            }
        });
    });

    $('#table').on('click', '.btn-delete-account', function() {
        var _htmlBtn = $(this).html();
        var profil_id = $(this).data('id');
        swal("Apakah anda yakin untuk menghapus profil ini?", {
            buttons: {
                cancel: "Tidak", 
                yes: "Ya",
            },
            icon: "warning",
            dangerMode: true,
        }).then((value) => {
            switch (value) {
                case ('yes'):
                    $.ajax({
                        type: 'GET',
                        url: window.location.origin + '/info-manager/delete-profil?id=' + profil_id,
                        // dataType: 'JSON',
                        beforeSend: function () {
                            $('.btn-delete-account').html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
                        },
                        success: function (res) {
                            $.notify("Hapus Profil berhasil.", "success");
                            $('.btn-delete-account').html(_htmlBtn).attr('disabled', false);
                            $('#table').DataTable().ajax.reload();
                        },
                        // complete: function () {
                        // }
                    });
                    break;
            }
        });
    } );
});



