
<?php
use yii\web\View; 
use yii\helpers\Html; 
$this->title = 'Detail Manager';
?>

<div class="box-header with-border">
    <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
</div>

<div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body box-profile">
                    <div class="col-md-3">
                        <img class="profile-user-img img-responsive img-circle" src="/img/profile/default.jpg" alt="User profile picture">

                        <h3 class="profile-username text-center"><?= $profil['nama_lengkap'] ?></h3>

                        <p class="text-muted text-center">SBC</p>
                    </div>
                    <div class="col-md-4">
                        <h4>Nama Lengkap</h4>
                        <h5 class="text-muted"><?= $profil['nama_lengkap']; ?></h5>
                        <h4>Alamat</h4>
                        <h5 class="text-muted"><?= $profil['alamat_sekarang']; ?></h5>
                        <h4>No Telepon</h4>
                        <h5 class="text-muted"><?= $profil['no_telepon']; ?></h5>
                    </div>
                    <div class="col-md-4">
                        <h4>Ruangan</h4>
                        <h5 class="text-muted"><?= $profil['managerRoom'] ? $profil['managerRoom']['nama_room'] : '-'; ?></h5>
                        <h4>Jadwal</h4>
                        <h5 class="text-muted"><?= $profil['managerRoom'] ? $profil['managerRoom']['jadwal_test'] : '-'; ?></h5>
                        <h4>Jumlah Peserta</h4>
                        <h5 class="text-muted"><?= count($followers); ?></h5>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
              <!-- /.box -->
        </div>
        <div class="col-md-12">
            <h4>List Anggota</h4>
            <table id="table" class="table table-bordered table-striped table-responsive" style="width: 100%">
                <thead>
                    <tr>
                      <th class="fixed-column">Nama Anggota</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-profile">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="modal-profile-content">
                
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php
$this->registerJs('
    var manager_id = ' . $profil['id'] . '
    ' . 
    $this->render('js/detail-manager.js'), View::POS_END);
?>
