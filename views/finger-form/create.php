<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\bootstrap\ActiveForm;
use yii\jui\DatePicker;
use kartik\select2\Select2;
use yii\web\JsExpression;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\models\FingerForm */
/* @var $form ActiveForm */
?>

<div class="box-header with-border">
  <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
</div>

<br>

<?php $form = ActiveForm::begin([
    // 'layout' => 'horizontal',
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-4',
            // 'offset' => 'col-sm-offset-4',
            'wrapper' => 'col-md-7 col-sm-8',
            'error' => '',
            'hint' => '',
        ],
    ],
]); ?>

<div class="box-body">
    <div class="finger-form-create">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'kantor')->dropDownList($listOffice) ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'nama_lengkap') ?>
                <?= $form->field($model, 'jenis_identitas')->inline()->radioList($listJenisId) ?>
                <?= $form->field($model, 'nomor_identitas') ?>
                <?= $form->field($model, 'tanggal_berlaku')->widget(DatePicker::class, [
                    //'language' => 'ru',
                    'dateFormat' => 'dd-MM-yyyy',
                    'options' => [
                        'class'=>'form-control',
                    ],
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                    ]
                ]) ?>
                <?= $form->field($model, 'alamat_identitas')->textArea() ?>
                <?= $form->field($model, 'rt_identitas') ?>
                <?= $form->field($model, 'rw_identitas') ?>
                <?= $form->field($model, 'provinsi_identitas')->dropDownList(
                    ArrayHelper::map($listProvinsi, 'nama', 'nama'),
                    [
                        'id'=>'propinsi_identitas',
                        'prompt'=>'— PILIH —',
                    ]
                ) ?>
                <?= $form->field($model, 'kota_identitas')->widget(DepDrop::classname(), [
                    'options'=>['id'=>'kota_identitas'],
                    'pluginOptions'=>[
                        'depends'=>['propinsi_identitas'],
                        'placeholder'=>'-- PILIH --',
                        'url'=>Url::to(['/finger-form/list-kota'])
                    ],
                ]) ?>
                <?= $form->field($model, 'kecamatan_identitas')->widget(DepDrop::classname(), [
                    'options'=>['id'=>'kecamatan_identitas'],
                    'pluginOptions'=>[
                        'depends'=>['kota_identitas'],
                        'placeholder'=>'-- PILIH --',
                        'url'=>Url::to(['/finger-form/list-kecamatan'])
                    ],
                ]) ?>
                <?= $form->field($model, 'kelurahan_identitas')->widget(DepDrop::classname(), [
                    'options'=>['id'=>'kelurahan_identitas'],
                    'pluginOptions'=>[
                        'depends'=>['kecamatan_identitas'],
                        'placeholder'=>'-- PILIH --',
                        'url'=>Url::to(['/finger-form/list-kelurahan'])
                    ],
                ]) ?>
                <?= $form->field($model, 'alamat_sekarang')->textArea() ?>
                <?= $form->field($model, 'rt_sekarang') ?>
                <?= $form->field($model, 'rw_sekarang') ?>
                <?= $form->field($model, 'provinsi_sekarang')->dropDownList(
                    ArrayHelper::map($listProvinsi, 'nama', 'nama'),
                    [
                        'id'=>'propinsi_sekarang',
                        'prompt'=>'— PILIH —',
                    ]
                ) ?>
                <?= $form->field($model, 'kota_sekarang')->widget(DepDrop::classname(), [
                    'options'=>['id'=>'kota_sekarang'],
                    'pluginOptions'=>[
                        'depends'=>['propinsi_sekarang'],
                        'placeholder'=>'-- PILIH --',
                        'url'=>Url::to(['/finger-form/list-kota'])
                    ],
                ]) ?>
                <?= $form->field($model, 'kecamatan_sekarang')->widget(DepDrop::classname(), [
                    'options'=>['id'=>'kecamatan_sekarang'],
                    'pluginOptions'=>[
                        'depends'=>['kota_sekarang'],
                        'placeholder'=>'-- PILIH --',
                        'url'=>Url::to(['/finger-form/list-kecamatan'])
                    ],
                ]) ?>
                <?= $form->field($model, 'kelurahan_sekarang')->widget(DepDrop::classname(), [
                    'options'=>['id'=>'kelurahan_sekarang'],
                    'pluginOptions'=>[
                        'depends'=>['kecamatan_sekarang'],
                        'placeholder'=>'-- PILIH --',
                        'url'=>Url::to(['/finger-form/list-kelurahan'])
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'tempat_lahir')->widget(Select2::classname(), [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => false,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Loading...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['/profil/get-kota']),
                            'dataType' => 'json',
                            'delay' => 500,
                            'data' => new JsExpression('
                                function(params) {
                                    return {
                                        q:params.term
                                    };
                                }
                            ')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        // 'templateResult' => new JsExpression(
                        //     'function(no_rekam_medik) {
                        //         return no_rekam_medik.text;
                        //     }'
                        // ),
                        // 'templateSelection' => new JsExpression(
                        //     'function (no_rekam_medik) {
                        //         $(".pendaftaran-id").val(no_rekam_medik.pendaftaran_id);
                        //         return no_rekam_medik.text;
                        //     }'
                        // ),
                    ],
                ]); ?>
                <?= $form->field($model, 'tanggal_lahir')->widget(DatePicker::class, [
                    //'language' => 'ru',
                    'dateFormat' => 'dd-MM-yyyy',
                    'options' => [
                        'class'=>'form-control',
                    ],
                    'clientOptions' => [
                        'changeMonth' => true,

                        'yearRange' => '1970:2099',
                        'changeYear' => true,
                    ]
                ]) ?>
                <?= $form->field($model, 'pendidikan_terakhir')->dropDownList($listPendidikanTerakhir) ?>
                <?= $form->field($model, 'jenis_kelamin')->inline()->radioList($listJenisKelamin) ?>
                <?= $form->field($model, 'status_perkawinan')->dropDownList($listStatusPerkawinan) ?>
                <?= $form->field($model, 'agama')->dropDownList($listAgama) ?>
                <?= $form->field($model, 'no_telepon') ?>
                <?= $form->field($model, 'no_handphone') ?>
                <?= $form->field($model, 'ayah_kandung') ?>
                <?= $form->field($model, 'ibu_kandung') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'keluarga_nama') ?>
                <?= $form->field($model, 'keluarga_alamat') ?>
                <?= $form->field($model, 'keluarga_no_telepon') ?>
                <?= $form->field($model, 'profil_id')->hiddenInput()->label(false); ?>
            </div>
        </div>  

    </div><!-- finger-form-create -->
</div>
<div class="box-footer">
    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary btn-lg']) ?>
    </div>
    
</div>

<?php ActiveForm::end(); ?>
