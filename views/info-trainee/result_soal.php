<?php 

use yii\web\View; 
use yii\helpers\Html; 
?>
<style type="text/css">
    .dl-horizontal dt {
        text-align: left;
    }
</style>

<div class="box box-success">
    <div class="box-header">
        
    </div>
    <div class="box-body">
        <div class="col-md-6">
            <table class="table table-responsive">
                <tbody>
                    <?php foreach ($successList as $key => $each): ?>
                        <tr>
                            <td><?= $each; ?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <table class="table table-responsive">
                <tbody>
                    <?php foreach ($failedList as $key => $each): ?>
                        <tr>
                            <td><?= $each; ?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
