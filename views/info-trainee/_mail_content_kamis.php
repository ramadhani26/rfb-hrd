
<?php
use yii\helpers\Html;
/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>

<?php $this->beginPage() ?>
<?php $this->beginBody() ?>
<p>
    Friendly reminder ☺ <br><br>

    Selamat kepada kandidat yang diterima sebagai Management Trainee dan Business Consultant. <br>
    Untuk proses selanjutnya dan pelatihan trading, diharapkan kehadirannya pada : <br>
        Hari/tanggal  : <?= $date ?> <br>
        Waktu             : 13.00 - selesai <br>
        Tempat           : Wisma Bumiputra lantai 3 Jalan Asia Afrika no 141-149 Bandung <br>
        Pakaian           : Bebas, Rapi dan Sopan <br>
        <br>
        <br>
        Terimakasih, good luck! <br>
        Budi Suhud <br>
        PT Rifan Financindo Bandung <br>
</p>

<?php $this->endBody() ?>
<?php $this->endPage() ?>