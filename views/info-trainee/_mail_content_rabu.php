
<?php
use yii\helpers\Html;
/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>

<?php $this->beginPage() ?>
<?php $this->beginBody() ?>
<p>
    Friendly reminder ☺ <br><br>

    Proses rekrutmen tahap selanjutnya yaitu Psikotest (tes tulis) yang akan dilaksanakan pada <br>
        Hari/tanggal  : <?= $date ?> <br>
        Waktu             : 13.00 - 16.00 <br>
        Tempat           : Wisma Bumiputra lantai 3 Jalan Asia Afrika no 141-149 Bandung <br>
<br>
        Membawa buku catatan dan alat tulis. Dan diharapkan untuk datang tepat waktu. <br>
        <br>
        <br>
        Terimakasih, good luck! <br>
        Budi Suhud <br>
        PT Rifan Financindo Bandung <br>
</p>

<?php $this->endBody() ?>
<?php $this->endPage() ?>