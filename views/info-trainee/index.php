<?php
use yii\web\View; 
use yii\helpers\Html; 
$this->title = 'Informasi Trainee';
?>

<div class="box-header with-border">
    <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
</div>

<div class="box-body">
    <table id="table" class="table table-bordered table-striped table-responsive">
        <thead>
            <tr>
                <th class="fixed-column">Nama Lengkap</th>
                <th>Alamat</th>
                <th>Tempat, Tgl Lahir</th>
                <th>Umur</th>
                <th>No Handphone</th>
                <th>SBC</th>
                <th>Room</th>
                <th>Status Trainee</th>
                <th>Status Soal</th>
                <th>Form Finger</th>
                <th>Drop SBC</th>
                <!-- <th>Aksi</th> -->
                <?php if (Yii::$app->user->identity->user_role_id != 3): ?>
                    <th>Hapus Akun</th>
                <?php endif ?>
                <!-- <th>Hapus Akun</th> -->
            </tr>
        </thead>
    </table>

    <?php if (Yii::$app->user->identity->user_role_id == 4): ?>
        <?= Html::a('Download Excel Fingerprint', '/info-trainee/download-excel', ['class'=>'btn btn-warning']); ?>
        <?= Html::button('Generate Hasil Soal', ['class'=>'btn btn-info btn-generate-hasil-soal']); ?>
    <?php endif ?>
</div>

<input type='text' id='txt_user_role_id' value='<?php echo Yii::$app->user->identity->user_role_id ?>' style='display:none' readonly />

<div class="modal fade" id="modal-profile">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="modal-profile-content">
                
            </div>
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php
$this->registerJs($this->render('js/index.js'), View::POS_END);
?>
