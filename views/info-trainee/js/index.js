$(function () {
  $('[data-toggle="popover"]').popover()
});
var table;
$(document).ready(function() {
    // var column = [];
    // column = [
    //     { 'data': 'nama_lengkap', 'sortable':false },
    //     { 'data': 'alamat_sekarang', 'sortable':false },
    //     { 'data': 'ttl', 'sortable':false },
    //     { 'data': 'umur', 'sortable':false },
    //     { 'data': 'no_handphone', 'sortable':false },
    //     { 'data': 'manager', 'sortable':false },
    //     { 'data': 'room', 'sortable':false },
    //     { 'data': 'status', 'sortable':false  },
    //     { 'data': 'status_soal', 'sortable':false  },
    //     { 'data': 'status_finger', 'sortable':false },
    //     { 'data': 'drop_manager', 'sortable':false },
    //     { 'data': 'aksi', 'sortable':false  },
    //     ];

    /// 3 = Manager
    if($("#txt_user_role_id").val() != "3"){
        table = $('#table').DataTable( {
            "ajax": "/info-trainee/get-data",
            "columnDefs": [
              { className: "dt-nowrap text-nowrap", "targets": [ 0, 2, 3, 4, 5, 6, 7, 8, 9, 10 ] }
            ],
            'paging' : true,
            'scrollX' : true,
            // 'scrollY' : true,
            "pagingType": "simple_numbers",
            "aaSorting": [],
            "columns": [
                { "data": "nama_lengkap", 'sortable':false },
                { "data": "alamat_sekarang", 'sortable':false },
                { "data": "ttl", 'sortable':false },
                { "data": "umur", 'sortable':false },
                { "data": "no_handphone", 'sortable':false },
                { "data": "manager", 'sortable':false },
                { "data": "room", 'sortable':false },
                { "data": "status", 'sortable':false  },
                { "data": "status_soal", 'sortable':false  },
                { "data": "status_finger", 'sortable':false },
                { "data": "drop_manager", 'sortable':false },
                // { "data": "aksi", 'sortable':false  },
                { "data": "akun", 'sortable':false  },
            ],
            drawCallback: function() {
                $('[data-toggle="popover"]').popover();
            }, 
            "rowCallback": function( row, data ) {
                $('#content-'+data.id).html('test');
                // console.log(data)
            }
        });
    }else{
        table = $('#table').DataTable( {
            "ajax": "/info-trainee/get-data",
            "columnDefs": [
              { className: "dt-nowrap text-nowrap", "targets": [ 0, 2, 3, 4, 5, 6, 7, 8, 9, 10 ] }
            ],
            'paging' : true,
            'scrollX' : true,
            // 'scrollY' : true,
            "pagingType": "simple_numbers",
            "aaSorting": [],
            "columns": [
                { "data": "nama_lengkap", 'sortable':false },
                { "data": "alamat_sekarang", 'sortable':false },
                { "data": "ttl", 'sortable':false },
                { "data": "umur", 'sortable':false },
                { "data": "no_handphone", 'sortable':false },
                { "data": "manager", 'sortable':false },
                { "data": "room", 'sortable':false },
                { "data": "status", 'sortable':false  },
                { "data": "status_soal", 'sortable':false  },
                { "data": "status_finger", 'sortable':false },
                { "data": "drop_manager", 'sortable':false },
                // { "data": "aksi", 'sortable':false  },
            ],
            drawCallback: function() {
                $('[data-toggle="popover"]').popover();
            }, 
            "rowCallback": function( row, data ) {
                $('#content-'+data.id).html('test');
                // console.log(data)
            }
        });
    }
    

    $('#table').on('click', '.btn-aktivasi', function() {
        var _htmlBtn = $(this).html();
        var profil_id = $(this).data('id');
        $.ajax({
            type: 'GET',
            url: window.location.origin + '/info-trainee/aktivasi?id=' + profil_id,
            // dataType: 'JSON',
            beforeSend: function () {
                $(this).html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
            },
            success: function (res) {
                $.notify("Aktivasi berhasil.", "success");
                $(this).html(_htmlBtn).attr('disabled', false);
                $('#table').DataTable().ajax.reload();
            },
            // complete: function () {
            // }
        });
    });

    $('#table').on('click', '.btn-approve-manager', function() {
        var _htmlBtn = $(this).html();
        var profil_id = $(this).data('id');
        var manager_id = $(this).data('managerId');
        $.ajax({
            type: 'GET',
            url: window.location.origin + '/info-trainee/approve-manager?id=' + profil_id + '&manager_id=' + manager_id,
            // dataType: 'JSON',
            beforeSend: function () {
                $(this).html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
            },
            success: function (res) {
                $.notify("Approve SBC berhasil.", "success");
                $(this).html(_htmlBtn).attr('disabled', false);
                $('#table').DataTable().ajax.reload();
            },
            // complete: function () {
            // }
        });
    });

    $('#table').on('click', '.btn-approve-recall', function() {
        var _htmlBtn = $('.btn-approve-recall').html();
        var profil_id = $(this).data('id');
        var manager_id = $(this).data('managerId');
        $.ajax({
            type: 'GET',
            url: window.location.origin + '/info-trainee/approve-recall?id=' + profil_id,
            // dataType: 'JSON',
            beforeSend: function () {
                $('.btn-approve-recall').html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
            },
            success: function (res) {
                $.notify("Approve Recall berhasil.", "success");
                $('.btn-approve-recall').html(_htmlBtn).attr('disabled', false);
                $('#table').DataTable().ajax.reload();
            },
            // complete: function () {
            // }
        });
    });

    $('#table').on('click', '.btn-delete-account', function() {
        var _htmlBtn = $(this).html();
        var profil_id = $(this).data('id');
        swal("Apakah anda yakin untuk menghapus profil ini?", {
            buttons: {
                cancel: "Tidak", 
                yes: "Ya",
            },
            icon: "warning",
            dangerMode: true,
        }).then((value) => {
            switch (value) {
                case ('yes'):
                    $.ajax({
                        type: 'GET',
                        url: window.location.origin + '/info-trainee/delete-profil?id=' + profil_id,
                        // dataType: 'JSON',
                        beforeSend: function () {
                            $('.btn-delete-account').html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
                        },
                        success: function (res) {
                            $.notify("Hapus Profil berhasil.", "success");
                            $('.btn-delete-account').html(_htmlBtn).attr('disabled', false);
                            $('#table').DataTable().ajax.reload();
                        },
                        // complete: function () {
                        // }
                    });
                    break;
            }
        });
    } );


    $('#table').on('click', '.btn-drop-manager', function() {
        var _this = $(this);
        var _htmlBtn = _this.html();
        var profil_id = _this.data('id');
        var manager_id = _this.data('managerId');
        $.ajax({
            type: 'GET',
            url: window.location.origin + '/info-trainee/drop-manager?id=' + profil_id,
            // dataType: 'JSON',
            beforeSend: function () {
                _this.html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
            },
            success: function (res) {
                $.notify("Drop SBC berhasil.", "success");
                _this.html(_htmlBtn).attr('disabled', false);
                $('#table').DataTable().ajax.reload();
            },
            // complete: function () {
            // }
        });
    });

    $('#table').on('click', '.btn-lolos-trainee', function() {
        var _this = $(this);
        var _htmlBtn = _this.html();
        var profil_id = _this.data('id');
        var manager_id = _this.data('managerId');
        $.ajax({
            type: 'GET',
            url: window.location.origin + '/info-trainee/finish-test?id=' + profil_id,
            // dataType: 'JSON',
            beforeSend: function () {
                _this.html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
            },
            success: function (res) {
                $.notify("Trainee telah lolos menjadi staff.", "success");
                _this.html(_htmlBtn).attr('disabled', false);
                $('#table').DataTable().ajax.reload();
            },
            // complete: function () {
            // }
        });
    });

    $('#table').on('click', '.btn-modal-reset-manager', function() {
        var profil_id = $(this).data('id');
        $('.modal-title').html('Pilih SBC');
        $.ajax({
            type: 'GET',
            url: window.location.origin + '/info-manager/modal-set-manager?id=' + profil_id,
            // dataType: 'JSON',
            beforeSend: function () {
                $('#modal-profile').modal();
                $('#modal-profile-content').html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div></br>');
            },
            success: function (res) {
                $('#modal-profile-content').html(res);
            },
            complete: function () {
            }
        });
    });

   $('#table').on('click', '.btn-profile-detail', function() {
        var profil_id = $(this).data('id');
        $('.modal-title').html('');
        $.ajax({
            type: 'GET',
            url: window.location.origin + '/profil/get-profil?id=' + profil_id,
            // dataType: 'JSON',
            beforeSend: function () {
                $('#modal-profile').modal();
                $('#modal-profile-content').html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div></br>');
            },
            success: function (res) {
                $('#modal-profile-content').html(res);
            },
            complete: function () {
            }
        });
    });
} );

$('.btn-generate-hasil-soal').click(function() {
    $('.modal-title').html('Hasil Pengisian Soal Trainee');
    var _this = $(this);
    var _htmlBtn = _this.html();
    // console.log(_htmlBtn); return false;
    var profil_id = _this.data('id');
    $.ajax({
        type: 'GET',
        url: window.location.origin + '/info-trainee/generate-hasil-soal',
        // dataType: 'JSON',
        beforeSend: function () {
            _this.html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);

            $('#modal-profile').modal();
            $('#modal-profile-content').html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div></br>');

        },
        success: function (res) {
            _this.html(_htmlBtn).attr('disabled', false);
            // $.notify("Request untuk recall berhasil.", "success");

            $('#modal-profile-content').html(res);
            $('#table').DataTable().ajax.reload();
        },
        // complete: function () {
        // }
    });
});
