$('.btn-profile-detail-2').click( function() {
    var profil_id = $(this).data('id');
    $('.modal-title').html('');
    $.ajax({
        type: 'GET',
        url: window.location.origin + '/profil/get-profil?id=' + profil_id,
        // dataType: 'JSON',
        beforeSend: function () {
            $('#modal-profile-2').modal();
            $('#modal-profile-2-content').html('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div></br>');
        },
        success: function (res) {
            $('#modal-profile-2-content').html(res);
        },
        complete: function () {
        }
    });
});