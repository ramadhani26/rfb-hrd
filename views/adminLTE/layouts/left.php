<?php 
use yii\helpers\Html;
use yii\helpers\Url;
 ?>
<style type="text/css">
     .main-sidebar {
        background-color: #00441a !important; 
     }
</style>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/img/profile/default.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?php echo Yii::$app->user->identity->username; ?></p>

                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Trainee</a> -->
                <div>
                    <?php if (Yii::$app->user->identity->user_role_id == 5) : ?>
                        <?php
                        $profil = new app\models\Profil;
                        $profil = $profil->find()
                            ->where(['user_id'=> Yii::$app->user->id])
                            ->one();
                        ?>
                        <?php
                        if ($profil) {
                            if ($profil->manager_approved == 1)  {
                                echo '<i class="fa fa-star"></i> ';
                                // echo '<i class="fa fa-star" data-toggle="tooltip" data-placement="bottom" title="Popover title" data-content="Right?"></i> ';
                            } else {
                                echo '<i class="fa fa-star-o"></i> ';
                            }

                            if ($profil->has_soal_submitted == 1) {
                                if ($profil->has_soal_approved == 1) {
                                    echo '<i class="fa fa-star"></i> ';
                                } elseif ($profil->has_soal_approved == 2) { // gagal di sesi soal
                                    if ($profil->response_recall != 1) {
                                        echo '<i class="fa fa-star-half-o"></i> ';
                                    } else {
                                        echo '<i class="fa fa-star"></i> ';
                                    }
                                } else {
                                    echo '<i class="fa fa-star-o"></i> ';
                                }
                            } else {
                                echo '<i class="fa fa-star-o"></i> ';
                            }

                            if ($profil->tanggal_finger) {
                                echo '<i class="fa fa-star"></i> ';
                            } else {
                                echo '<i class="fa fa-star-o"></i> ';
                            }

                            if ($profil->drop_manager) {
                                echo '<i class="fa fa-star"></i> ';
                            } else {
                                echo '<i class="fa fa-star-o"></i> ';
                            }

                        } else {
                            echo '<i class="fa fa-star-o"></i> ';
                            echo '<i class="fa fa-star-o"></i> ';
                            echo '<i class="fa fa-star-o"></i> ';
                            echo '<i class="fa fa-star-o"></i> ';
                        }
                        ?>
                    <?php else : ?>
                        <?php echo Yii::$app->user->identity->userRole->name; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    // ['label' => 'Home', 'icon' => 'home', 'url' => ['/'], 'visible'=>in_array(Yii::$app->user->identity->user_role_id, [4]) ? false : true],
                    ['label' => 'Home', 'icon' => 'home', 'visible'=>in_array(Yii::$app->user->identity->user_role_id, [4]) ? false : true, 'items'=>[
                        ['label' => 'Tentang Kami', 'icon' => '', 'url' => ['/'], 'visible'=>in_array(Yii::$app->user->identity->user_role_id, [3]) ? true : false],
                        ['label' => 'Tentang RFB Bandung', 'icon' => '', 'url' => ['/about-rfb']],
                        ['label' => 'Tentang Tim Jojo', 'icon' => '', 'url' => ['/about-jojo-team']],
                        ['label' => 'Penerimaan Karyawan', 'icon' => '', 'url' => ['/site/sistem-penerimaan-karyawan']],
                        ['label' => 'Produk', 'icon' => '', 'url' => ['/site/produk']],
                        ['label' => 'Legalitas', 'icon' => '', 'url' => ['/site/legalitas']],
                        ['label' => 'Info & Kegiatan Kami', 'icon' => '', 'url' => ['/site/kegiatan-kami']],
                    ]],
                    ['label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/dashboard'], 'visible'=>in_array(Yii::$app->user->identity->user_role_id, [4]) ? true : false],
                    ['label' => in_array(Yii::$app->user->identity->user_role_id, [5]) ? 'Profil' : 'Team', 'icon' => 'user', 'url' => ['/profil']],

                    ['label' => 'Info', 'icon' => 'dashboard', 'items' => [
                        ['label' => 'Trainee', 'icon' => '', 'url' => ['/info-trainee'], 'visible'=>in_array(Yii::$app->user->identity->user_role_id, [3, 4]) ? true : false],
                        ['label' => 'SBC & MT/BC', 'icon' => '', 'url' => ['/info-manager'], 'visible'=>in_array(Yii::$app->user->identity->user_role_id, [4]) ? true : false],
                        ['label' => 'Room', 'icon' => '', 'url' => ['/info-room'], 'visible'=>in_array(Yii::$app->user->identity->user_role_id, [4]) ? true : false],
                    ]],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    // [
                    //     'label' => 'Some tools',
                    //     'icon' => 'share',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                    //         ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                    //         [
                    //             'label' => 'Level One',
                    //             'icon' => 'circle-o',
                    //             'url' => '#',
                    //             'items' => [
                    //                 ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                    //                 [
                    //                     'label' => 'Level Two',
                    //                     'icon' => 'circle-o',
                    //                     'url' => '#',
                    //                     'items' => [
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                     ],
                    //                 ],
                    //             ],
                    //         ],
                    //     ],
                    // ],
                ],
            ]
        ) ?>

    </section>

</aside>
