<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use dmstr\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

dmstr\web\AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">
        .login-page {
            width: 100%;
            height: 100vh;
            background-position: center center;
            background-size: cover;
            background-repeat: no-repeat;
            backface-visibility: hidden;
            animation: slideBg 60s linear infinite 0s;
            animation-timing-function: ease-in-out;
            background-image: url('/img/bg/1.jpg');
            color: white !important;
        }
        @keyframes slideBg {
            0% {
                background-image: url('/img/bg/1.jpg');
            }
            20% {
                background-image: url('/img/bg/2.jpg');
            }
            40% {
                background-image: url('/img/bg/3.jpg');
            }
            60% {
                background-image: url('/img/bg/4.jpg');
            }
            80% {
                background-image: url('/img/bg/5.jpg');
            }
            100% {
                background-image: url('/img/bg/6.jpg');
            }
        }

        .login-logo {
            color: #666666 !important;
        }
        .login-box-body {
            /*opacity: 0.5;*/
            background-color: rgba(255, 255, 255, 0.5);
        }

    </style>
</head>
<body class="login-page">

<?php $this->beginBody() ?>
    <div class="login-box">
        <?= Alert::widget() ?>
        <div class="login-logo">
            <b>PT. RFB</b> - Bandung
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <?= $content ?>
        </div>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
