<?php

use yii\web\View;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\BreadCrumbs;

/* @var $this yii\web\View */
/* @var $model app\models\Profil */
/* @var $form ActiveForm */
$this->title = 'Sesi Pengisian Soal';
?>
<style type="text/css">
    .form-group.required .control-label:after {
      content:" *";
      color:red;
    }
</style>

<div class="box-header with-border">
  <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
</div>

<div class="box-body">
    <!-- form start -->
    <?= Html::beginForm(['/sesi-soal/submit'], 'POST' , ['id'=>'soal-form']); ?>

        <?php 
        echo Html::hiddenInput('profil_id', $profil->id);
        foreach ($questions as $key => $question) : ?>
            <?php 
            $required = $question->required ? 'required' : '';
            ?>
            <div class="form-group <?= $required; ?> <?= 'form-group-'.$question->urutan ?>">
                <label class="control-label"><?= $question->pertanyaan; ?></label>
                <?php 
                switch ($question->jenis) {
                    case 'text':
                        echo Html::textarea($question->id, '', ['class'=>'form-control', 'id'=>$question->id]);
                        break;
                    case 'checkboxlist':
                        echo Html::checkboxlist($question->id, [], $choices[$question->l_type], [
                            'class'=>'checkbox', 
                            'separator' => '<br>', 
                            'id'=>$question->id, 
                            'style'=>'height: 100%;'
                        ]);
                        break;
                    case 'dropdownlist':
                        echo Html::dropdownlist($question->id, [], $choices[$question->l_type], ['class'=>'form-control', 'id'=>$question->id]);
                        break;
                    case 'radiobuttonlist':
                        echo Html::radiolist($question->id, [], $choices[$question->l_type], ['id'=>$question->id]);
                        break;
                    
                    default:
                        # code...
                        break;
                }
                ?>
                <div class="col-md-7 col-sm-8">
                    <p class="help-block help-block-error <?= 'error-'.$question->urutan ?>"></p>
                </div>
            </div>
            <br>
        <?php endforeach; ?>


        <div class="form-group">
            <?= Html::button('Submit Jawaban', ['class' => 'btn btn-primary btn-lg btn-simpan-jawaban']); ?>
        </div>
    <?= Html::endForm(); ?>
</div>


<?php
$this->registerJs($this->render('js/isi.js'), View::POS_END);
?>