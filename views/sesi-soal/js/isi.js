// swal("Here's the title!", "...and here's the text!");
$('.btn-simpan-jawaban').click(function() {
    swal("Apakah anda yakin untuk submit jawaban?", {
        buttons: {
            cancel: "Tidak", 
            yes: "Ya",
        }
    }).then((value) => {
        switch (value) {
            case ('yes'):
                // swal('ajax here');
                submitJawaban();
                break;
        }
    });
});

function submitJawaban() {
    var tempHtmlBtn = $('.btn-simpan-jawaban').html();
    var data = $('#soal-form').serializeArray();
    $.ajax({
        type: 'POST',
        url: window.location.origin + '/sesi-soal/submit-jawaban',
        data: data,
        dataType: 'JSON',
        beforeSend: function () {
            $('.form-group').removeClass('has-error');
            $('.help-block-error').html('');
            $('.btn-simpan-jawaban').html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
        },
        success: function (res) {
            if (res.status == 422) {
                $.notify("Silakan cek inputan", "error");
                $.each(res.errors, function(i, val) {
                    $('.form-group-'+val.id).addClass('has-error');
                    $('.error-'+val.id).html(val.message);
                });
            } else {
                swal("Terima kasih. Jawaban soal sudah tersimpan. Halaman akan pindah ke profil", {
                    icon: "success",
                    buttons: {
                        yes: true
                    }
                }).then((value) => {
                    switch (value) {
                        case ('yes'):
                            window.location.href = '/profil/index';
                            break;
                    }
                });
            }
        },
        complete: function () {
            $('.btn-simpan-jawaban').html(tempHtmlBtn).attr('disabled', false);
        }
    });
}