var table;
$(document).ready(function() {
    table = $('#table').DataTable( {
        "ajax": "/info-room/get-data",
        "columnDefs": [
          { className: "dt-nowrap text-nowrap", "targets": [ 0, 1, 2, 3] }
        ],
        'paging' : true,
        'scrollX' : true,
        "bAutoWidth": false,
        // 'scrollY' : true,
        "pagingType": "simple_numbers",
        "columns": [
            { "data": "nama_room" },
            { "data": "jadwal_test" },
            { "data": "list_manager", 'sortable':false },
            { "data": "aksi", 'sortable':false  },  
        ]
    } );

    $('#table').on('click', '.btn-blast-soal', function() {
        var _this = $(this);
        var _htmlBtn = _this.html();
        var profil_id = _this.data('id');
        $.ajax({
            type: 'GET',
            url: window.location.origin + '/info-room/show-soal?id=' + profil_id,
            // dataType: 'JSON',
            beforeSend: function () {
                _this.html('<i class="fa fa-refresh fa-spin"></i> Loading').attr('disabled', true);
            },
            success: function (res) {
                $.notify("Soal berhasil ditampilkan untuk ruangan " + res.room.nama_room, "success");
                _this.html(_htmlBtn).attr('disabled', false);
                $('#table').DataTable().ajax.reload();
            },
            // complete: function () {
            // }
        });
    });
});