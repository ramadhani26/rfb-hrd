<?php
use yii\web\View; 
use yii\helpers\Html; 
$this->title = 'Informasi Ruangan';
?>

<div class="box-header with-border">
    <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
</div>

<div class="box-body">
    <table id="table" class="table table-bordered table-striped table-responsive">
        <thead>
            <tr>
              <th class="fixed-column">Nama Room</th>
              <th>Jadwal Test</th>
              <th>SBC</th>
              <th>Aksi</th>
              <!-- <th>Hapus Akun</th> -->
            </tr>
        </thead>
    </table>
</div>


<?php
$this->registerJs($this->render('js/index.js'), View::POS_END);
?>
