<?php

/* @var $this yii\web\View */

$this->title = 'About Jojo Team';
?>
<div class="site-index">
    <div class="box-header with-border">
        <h3 class="box-title"><?= isset($title) ? $title : $this->title; ?></h3>
    </div>

    <div class="box-body" style="height: 90vh;">
        <p>
        Tim Yudi Permana (Vice Branch Manager) merupakan salah satu tim konsultan profesional yang menjadi bagian dari RFB Bandung. Tim yang terbentuk sejak tahun 2009 ini 4 (Empat) tim utama yang berisikan staff-staff konsultan profesional yang selalu siap membantu para investor untuk menjalankan kegiatan perdagangan berjangka komoditi. Terdiri dari mayoritas anak usia muda (21-35 tahun) tim Yudi Permana atau sering disapa Jojo ini menjadi tim yang didukung dengan semangat bekerja tinggi dan pencapaian luar biasa dari setiap lini mulai dari pencapaian kerja bulanan, pencapaian kerja quartalan, hingga pencapaian kerja tahunan atas dukungan dari 2 VBM under langsung dan 4 tim besar sebagai berikut:
        <br/>
        <b>
        <br/>VBM 1: Aang Maryana
        <br/>VBM 2: M. Luthfi Jundiaturridwan
        <br/>Tim RF05: Dipimpin oleh Nurul Rahmi
        <br/>Tim RF07: Dipimpin oleh Gamma Abdillah dan Gerri Sugiamukti
        <br/>Tim RF08: Dipimpin oleh Mega Sukma Rahayu
        <br/>Tim RF10: Dipimpin oleh Maulida Puji dan Cici S Liningsih
        </b>
        </p>
    </div>
</div>
